/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { NodeCommodityDetailComponent } from 'app/entities/node-commodity/node-commodity-detail.component';
import { NodeCommodity } from 'app/shared/model/node-commodity.model';

describe('Component Tests', () => {
    describe('NodeCommodity Management Detail Component', () => {
        let comp: NodeCommodityDetailComponent;
        let fixture: ComponentFixture<NodeCommodityDetailComponent>;
        const route = ({ data: of({ nodeCommodity: new NodeCommodity(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [NodeCommodityDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(NodeCommodityDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(NodeCommodityDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.nodeCommodity).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
