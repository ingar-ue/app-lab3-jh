/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLabTestModule } from '../../../test.module';
import { NodeCommodityDeleteDialogComponent } from 'app/entities/node-commodity/node-commodity-delete-dialog.component';
import { NodeCommodityService } from 'app/entities/node-commodity/node-commodity.service';

describe('Component Tests', () => {
    describe('NodeCommodity Management Delete Component', () => {
        let comp: NodeCommodityDeleteDialogComponent;
        let fixture: ComponentFixture<NodeCommodityDeleteDialogComponent>;
        let service: NodeCommodityService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [NodeCommodityDeleteDialogComponent]
            })
                .overrideTemplate(NodeCommodityDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(NodeCommodityDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(NodeCommodityService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
