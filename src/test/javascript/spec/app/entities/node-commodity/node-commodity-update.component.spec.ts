/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { NodeCommodityUpdateComponent } from 'app/entities/node-commodity/node-commodity-update.component';
import { NodeCommodityService } from 'app/entities/node-commodity/node-commodity.service';
import { NodeCommodity } from 'app/shared/model/node-commodity.model';

describe('Component Tests', () => {
    describe('NodeCommodity Management Update Component', () => {
        let comp: NodeCommodityUpdateComponent;
        let fixture: ComponentFixture<NodeCommodityUpdateComponent>;
        let service: NodeCommodityService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [NodeCommodityUpdateComponent]
            })
                .overrideTemplate(NodeCommodityUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(NodeCommodityUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(NodeCommodityService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new NodeCommodity(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.nodeCommodity = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new NodeCommodity();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.nodeCommodity = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
