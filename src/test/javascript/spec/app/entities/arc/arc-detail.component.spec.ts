/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ArcDetailComponent } from 'app/entities/arc/arc-detail.component';
import { Arc } from 'app/shared/model/arc.model';

describe('Component Tests', () => {
    describe('Arc Management Detail Component', () => {
        let comp: ArcDetailComponent;
        let fixture: ComponentFixture<ArcDetailComponent>;
        const route = ({ data: of({ arc: new Arc(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ArcDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ArcDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ArcDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.arc).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
