/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ArcUpdateComponent } from 'app/entities/arc/arc-update.component';
import { ArcService } from 'app/entities/arc/arc.service';
import { Arc } from 'app/shared/model/arc.model';

describe('Component Tests', () => {
    describe('Arc Management Update Component', () => {
        let comp: ArcUpdateComponent;
        let fixture: ComponentFixture<ArcUpdateComponent>;
        let service: ArcService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ArcUpdateComponent]
            })
                .overrideTemplate(ArcUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ArcUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ArcService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Arc(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.arc = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Arc();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.arc = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
