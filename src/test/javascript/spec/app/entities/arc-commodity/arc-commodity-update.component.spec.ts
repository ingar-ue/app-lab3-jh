/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ArcCommodityUpdateComponent } from 'app/entities/arc-commodity/arc-commodity-update.component';
import { ArcCommodityService } from 'app/entities/arc-commodity/arc-commodity.service';
import { ArcCommodity } from 'app/shared/model/arc-commodity.model';

describe('Component Tests', () => {
    describe('ArcCommodity Management Update Component', () => {
        let comp: ArcCommodityUpdateComponent;
        let fixture: ComponentFixture<ArcCommodityUpdateComponent>;
        let service: ArcCommodityService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ArcCommodityUpdateComponent]
            })
                .overrideTemplate(ArcCommodityUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ArcCommodityUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ArcCommodityService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new ArcCommodity(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.arcCommodity = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new ArcCommodity();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.arcCommodity = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
