/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLabTestModule } from '../../../test.module';
import { ArcCommodityDetailComponent } from 'app/entities/arc-commodity/arc-commodity-detail.component';
import { ArcCommodity } from 'app/shared/model/arc-commodity.model';

describe('Component Tests', () => {
    describe('ArcCommodity Management Detail Component', () => {
        let comp: ArcCommodityDetailComponent;
        let fixture: ComponentFixture<ArcCommodityDetailComponent>;
        const route = ({ data: of({ arcCommodity: new ArcCommodity(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ArcCommodityDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ArcCommodityDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ArcCommodityDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.arcCommodity).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
