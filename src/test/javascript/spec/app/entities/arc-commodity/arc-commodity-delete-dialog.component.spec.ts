/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLabTestModule } from '../../../test.module';
import { ArcCommodityDeleteDialogComponent } from 'app/entities/arc-commodity/arc-commodity-delete-dialog.component';
import { ArcCommodityService } from 'app/entities/arc-commodity/arc-commodity.service';

describe('Component Tests', () => {
    describe('ArcCommodity Management Delete Component', () => {
        let comp: ArcCommodityDeleteDialogComponent;
        let fixture: ComponentFixture<ArcCommodityDeleteDialogComponent>;
        let service: ArcCommodityService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLabTestModule],
                declarations: [ArcCommodityDeleteDialogComponent]
            })
                .overrideTemplate(ArcCommodityDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ArcCommodityDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ArcCommodityService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
