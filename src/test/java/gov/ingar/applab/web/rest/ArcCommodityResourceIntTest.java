package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.ArcCommodity;
import gov.ingar.applab.repository.ArcCommodityRepository;
import gov.ingar.applab.service.ArcCommodityService;
import gov.ingar.applab.service.dto.ArcCommodityDTO;
import gov.ingar.applab.service.mapper.ArcCommodityMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ArcCommodityResource REST controller.
 *
 * @see ArcCommodityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class ArcCommodityResourceIntTest {

    private static final Double DEFAULT_COST = 1D;
    private static final Double UPDATED_COST = 2D;

    private static final Integer DEFAULT_CAPACITY = 1;
    private static final Integer UPDATED_CAPACITY = 2;

    @Autowired
    private ArcCommodityRepository arcCommodityRepository;

    @Autowired
    private ArcCommodityMapper arcCommodityMapper;

    @Autowired
    private ArcCommodityService arcCommodityService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restArcCommodityMockMvc;

    private ArcCommodity arcCommodity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ArcCommodityResource arcCommodityResource = new ArcCommodityResource(arcCommodityService);
        this.restArcCommodityMockMvc = MockMvcBuilders.standaloneSetup(arcCommodityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ArcCommodity createEntity(EntityManager em) {
        ArcCommodity arcCommodity = new ArcCommodity()
            .cost(DEFAULT_COST)
            .capacity(DEFAULT_CAPACITY);
        return arcCommodity;
    }

    @Before
    public void initTest() {
        arcCommodity = createEntity(em);
    }

    @Test
    @Transactional
    public void createArcCommodity() throws Exception {
        int databaseSizeBeforeCreate = arcCommodityRepository.findAll().size();

        // Create the ArcCommodity
        ArcCommodityDTO arcCommodityDTO = arcCommodityMapper.toDto(arcCommodity);
        restArcCommodityMockMvc.perform(post("/api/arc-commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcCommodityDTO)))
            .andExpect(status().isCreated());

        // Validate the ArcCommodity in the database
        List<ArcCommodity> arcCommodityList = arcCommodityRepository.findAll();
        assertThat(arcCommodityList).hasSize(databaseSizeBeforeCreate + 1);
        ArcCommodity testArcCommodity = arcCommodityList.get(arcCommodityList.size() - 1);
        assertThat(testArcCommodity.getCost()).isEqualTo(DEFAULT_COST);
        assertThat(testArcCommodity.getCapacity()).isEqualTo(DEFAULT_CAPACITY);
    }

    @Test
    @Transactional
    public void createArcCommodityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = arcCommodityRepository.findAll().size();

        // Create the ArcCommodity with an existing ID
        arcCommodity.setId(1L);
        ArcCommodityDTO arcCommodityDTO = arcCommodityMapper.toDto(arcCommodity);

        // An entity with an existing ID cannot be created, so this API call must fail
        restArcCommodityMockMvc.perform(post("/api/arc-commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcCommodityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ArcCommodity in the database
        List<ArcCommodity> arcCommodityList = arcCommodityRepository.findAll();
        assertThat(arcCommodityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCostIsRequired() throws Exception {
        int databaseSizeBeforeTest = arcCommodityRepository.findAll().size();
        // set the field null
        arcCommodity.setCost(null);

        // Create the ArcCommodity, which fails.
        ArcCommodityDTO arcCommodityDTO = arcCommodityMapper.toDto(arcCommodity);

        restArcCommodityMockMvc.perform(post("/api/arc-commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcCommodityDTO)))
            .andExpect(status().isBadRequest());

        List<ArcCommodity> arcCommodityList = arcCommodityRepository.findAll();
        assertThat(arcCommodityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCapacityIsRequired() throws Exception {
        int databaseSizeBeforeTest = arcCommodityRepository.findAll().size();
        // set the field null
        arcCommodity.setCapacity(null);

        // Create the ArcCommodity, which fails.
        ArcCommodityDTO arcCommodityDTO = arcCommodityMapper.toDto(arcCommodity);

        restArcCommodityMockMvc.perform(post("/api/arc-commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcCommodityDTO)))
            .andExpect(status().isBadRequest());

        List<ArcCommodity> arcCommodityList = arcCommodityRepository.findAll();
        assertThat(arcCommodityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllArcCommodities() throws Exception {
        // Initialize the database
        arcCommodityRepository.saveAndFlush(arcCommodity);

        // Get all the arcCommodityList
        restArcCommodityMockMvc.perform(get("/api/arc-commodities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(arcCommodity.getId().intValue())))
            .andExpect(jsonPath("$.[*].cost").value(hasItem(DEFAULT_COST.doubleValue())))
            .andExpect(jsonPath("$.[*].capacity").value(hasItem(DEFAULT_CAPACITY)));
    }
    
    @Test
    @Transactional
    public void getArcCommodity() throws Exception {
        // Initialize the database
        arcCommodityRepository.saveAndFlush(arcCommodity);

        // Get the arcCommodity
        restArcCommodityMockMvc.perform(get("/api/arc-commodities/{id}", arcCommodity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(arcCommodity.getId().intValue()))
            .andExpect(jsonPath("$.cost").value(DEFAULT_COST.doubleValue()))
            .andExpect(jsonPath("$.capacity").value(DEFAULT_CAPACITY));
    }

    @Test
    @Transactional
    public void getNonExistingArcCommodity() throws Exception {
        // Get the arcCommodity
        restArcCommodityMockMvc.perform(get("/api/arc-commodities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateArcCommodity() throws Exception {
        // Initialize the database
        arcCommodityRepository.saveAndFlush(arcCommodity);

        int databaseSizeBeforeUpdate = arcCommodityRepository.findAll().size();

        // Update the arcCommodity
        ArcCommodity updatedArcCommodity = arcCommodityRepository.findById(arcCommodity.getId()).get();
        // Disconnect from session so that the updates on updatedArcCommodity are not directly saved in db
        em.detach(updatedArcCommodity);
        updatedArcCommodity
            .cost(UPDATED_COST)
            .capacity(UPDATED_CAPACITY);
        ArcCommodityDTO arcCommodityDTO = arcCommodityMapper.toDto(updatedArcCommodity);

        restArcCommodityMockMvc.perform(put("/api/arc-commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcCommodityDTO)))
            .andExpect(status().isOk());

        // Validate the ArcCommodity in the database
        List<ArcCommodity> arcCommodityList = arcCommodityRepository.findAll();
        assertThat(arcCommodityList).hasSize(databaseSizeBeforeUpdate);
        ArcCommodity testArcCommodity = arcCommodityList.get(arcCommodityList.size() - 1);
        assertThat(testArcCommodity.getCost()).isEqualTo(UPDATED_COST);
        assertThat(testArcCommodity.getCapacity()).isEqualTo(UPDATED_CAPACITY);
    }

    @Test
    @Transactional
    public void updateNonExistingArcCommodity() throws Exception {
        int databaseSizeBeforeUpdate = arcCommodityRepository.findAll().size();

        // Create the ArcCommodity
        ArcCommodityDTO arcCommodityDTO = arcCommodityMapper.toDto(arcCommodity);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restArcCommodityMockMvc.perform(put("/api/arc-commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcCommodityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ArcCommodity in the database
        List<ArcCommodity> arcCommodityList = arcCommodityRepository.findAll();
        assertThat(arcCommodityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteArcCommodity() throws Exception {
        // Initialize the database
        arcCommodityRepository.saveAndFlush(arcCommodity);

        int databaseSizeBeforeDelete = arcCommodityRepository.findAll().size();

        // Get the arcCommodity
        restArcCommodityMockMvc.perform(delete("/api/arc-commodities/{id}", arcCommodity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ArcCommodity> arcCommodityList = arcCommodityRepository.findAll();
        assertThat(arcCommodityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ArcCommodity.class);
        ArcCommodity arcCommodity1 = new ArcCommodity();
        arcCommodity1.setId(1L);
        ArcCommodity arcCommodity2 = new ArcCommodity();
        arcCommodity2.setId(arcCommodity1.getId());
        assertThat(arcCommodity1).isEqualTo(arcCommodity2);
        arcCommodity2.setId(2L);
        assertThat(arcCommodity1).isNotEqualTo(arcCommodity2);
        arcCommodity1.setId(null);
        assertThat(arcCommodity1).isNotEqualTo(arcCommodity2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ArcCommodityDTO.class);
        ArcCommodityDTO arcCommodityDTO1 = new ArcCommodityDTO();
        arcCommodityDTO1.setId(1L);
        ArcCommodityDTO arcCommodityDTO2 = new ArcCommodityDTO();
        assertThat(arcCommodityDTO1).isNotEqualTo(arcCommodityDTO2);
        arcCommodityDTO2.setId(arcCommodityDTO1.getId());
        assertThat(arcCommodityDTO1).isEqualTo(arcCommodityDTO2);
        arcCommodityDTO2.setId(2L);
        assertThat(arcCommodityDTO1).isNotEqualTo(arcCommodityDTO2);
        arcCommodityDTO1.setId(null);
        assertThat(arcCommodityDTO1).isNotEqualTo(arcCommodityDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(arcCommodityMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(arcCommodityMapper.fromId(null)).isNull();
    }
}
