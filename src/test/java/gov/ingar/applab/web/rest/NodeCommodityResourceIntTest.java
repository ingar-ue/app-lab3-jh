package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.NodeCommodity;
import gov.ingar.applab.repository.NodeCommodityRepository;
import gov.ingar.applab.service.NodeCommodityService;
import gov.ingar.applab.service.dto.NodeCommodityDTO;
import gov.ingar.applab.service.mapper.NodeCommodityMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NodeCommodityResource REST controller.
 *
 * @see NodeCommodityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class NodeCommodityResourceIntTest {

    private static final Integer DEFAULT_SUPPLY_DEMAND = 1;
    private static final Integer UPDATED_SUPPLY_DEMAND = 2;

    @Autowired
    private NodeCommodityRepository nodeCommodityRepository;

    @Autowired
    private NodeCommodityMapper nodeCommodityMapper;

    @Autowired
    private NodeCommodityService nodeCommodityService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNodeCommodityMockMvc;

    private NodeCommodity nodeCommodity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NodeCommodityResource nodeCommodityResource = new NodeCommodityResource(nodeCommodityService);
        this.restNodeCommodityMockMvc = MockMvcBuilders.standaloneSetup(nodeCommodityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NodeCommodity createEntity(EntityManager em) {
        NodeCommodity nodeCommodity = new NodeCommodity()
            .supplyDemand(DEFAULT_SUPPLY_DEMAND);
        return nodeCommodity;
    }

    @Before
    public void initTest() {
        nodeCommodity = createEntity(em);
    }

    @Test
    @Transactional
    public void createNodeCommodity() throws Exception {
        int databaseSizeBeforeCreate = nodeCommodityRepository.findAll().size();

        // Create the NodeCommodity
        NodeCommodityDTO nodeCommodityDTO = nodeCommodityMapper.toDto(nodeCommodity);
        restNodeCommodityMockMvc.perform(post("/api/node-commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nodeCommodityDTO)))
            .andExpect(status().isCreated());

        // Validate the NodeCommodity in the database
        List<NodeCommodity> nodeCommodityList = nodeCommodityRepository.findAll();
        assertThat(nodeCommodityList).hasSize(databaseSizeBeforeCreate + 1);
        NodeCommodity testNodeCommodity = nodeCommodityList.get(nodeCommodityList.size() - 1);
        assertThat(testNodeCommodity.getSupplyDemand()).isEqualTo(DEFAULT_SUPPLY_DEMAND);
    }

    @Test
    @Transactional
    public void createNodeCommodityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nodeCommodityRepository.findAll().size();

        // Create the NodeCommodity with an existing ID
        nodeCommodity.setId(1L);
        NodeCommodityDTO nodeCommodityDTO = nodeCommodityMapper.toDto(nodeCommodity);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNodeCommodityMockMvc.perform(post("/api/node-commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nodeCommodityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NodeCommodity in the database
        List<NodeCommodity> nodeCommodityList = nodeCommodityRepository.findAll();
        assertThat(nodeCommodityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkSupplyDemandIsRequired() throws Exception {
        int databaseSizeBeforeTest = nodeCommodityRepository.findAll().size();
        // set the field null
        nodeCommodity.setSupplyDemand(null);

        // Create the NodeCommodity, which fails.
        NodeCommodityDTO nodeCommodityDTO = nodeCommodityMapper.toDto(nodeCommodity);

        restNodeCommodityMockMvc.perform(post("/api/node-commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nodeCommodityDTO)))
            .andExpect(status().isBadRequest());

        List<NodeCommodity> nodeCommodityList = nodeCommodityRepository.findAll();
        assertThat(nodeCommodityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNodeCommodities() throws Exception {
        // Initialize the database
        nodeCommodityRepository.saveAndFlush(nodeCommodity);

        // Get all the nodeCommodityList
        restNodeCommodityMockMvc.perform(get("/api/node-commodities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nodeCommodity.getId().intValue())))
            .andExpect(jsonPath("$.[*].supplyDemand").value(hasItem(DEFAULT_SUPPLY_DEMAND)));
    }
    
    @Test
    @Transactional
    public void getNodeCommodity() throws Exception {
        // Initialize the database
        nodeCommodityRepository.saveAndFlush(nodeCommodity);

        // Get the nodeCommodity
        restNodeCommodityMockMvc.perform(get("/api/node-commodities/{id}", nodeCommodity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(nodeCommodity.getId().intValue()))
            .andExpect(jsonPath("$.supplyDemand").value(DEFAULT_SUPPLY_DEMAND));
    }

    @Test
    @Transactional
    public void getNonExistingNodeCommodity() throws Exception {
        // Get the nodeCommodity
        restNodeCommodityMockMvc.perform(get("/api/node-commodities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNodeCommodity() throws Exception {
        // Initialize the database
        nodeCommodityRepository.saveAndFlush(nodeCommodity);

        int databaseSizeBeforeUpdate = nodeCommodityRepository.findAll().size();

        // Update the nodeCommodity
        NodeCommodity updatedNodeCommodity = nodeCommodityRepository.findById(nodeCommodity.getId()).get();
        // Disconnect from session so that the updates on updatedNodeCommodity are not directly saved in db
        em.detach(updatedNodeCommodity);
        updatedNodeCommodity
            .supplyDemand(UPDATED_SUPPLY_DEMAND);
        NodeCommodityDTO nodeCommodityDTO = nodeCommodityMapper.toDto(updatedNodeCommodity);

        restNodeCommodityMockMvc.perform(put("/api/node-commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nodeCommodityDTO)))
            .andExpect(status().isOk());

        // Validate the NodeCommodity in the database
        List<NodeCommodity> nodeCommodityList = nodeCommodityRepository.findAll();
        assertThat(nodeCommodityList).hasSize(databaseSizeBeforeUpdate);
        NodeCommodity testNodeCommodity = nodeCommodityList.get(nodeCommodityList.size() - 1);
        assertThat(testNodeCommodity.getSupplyDemand()).isEqualTo(UPDATED_SUPPLY_DEMAND);
    }

    @Test
    @Transactional
    public void updateNonExistingNodeCommodity() throws Exception {
        int databaseSizeBeforeUpdate = nodeCommodityRepository.findAll().size();

        // Create the NodeCommodity
        NodeCommodityDTO nodeCommodityDTO = nodeCommodityMapper.toDto(nodeCommodity);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNodeCommodityMockMvc.perform(put("/api/node-commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nodeCommodityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NodeCommodity in the database
        List<NodeCommodity> nodeCommodityList = nodeCommodityRepository.findAll();
        assertThat(nodeCommodityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNodeCommodity() throws Exception {
        // Initialize the database
        nodeCommodityRepository.saveAndFlush(nodeCommodity);

        int databaseSizeBeforeDelete = nodeCommodityRepository.findAll().size();

        // Get the nodeCommodity
        restNodeCommodityMockMvc.perform(delete("/api/node-commodities/{id}", nodeCommodity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<NodeCommodity> nodeCommodityList = nodeCommodityRepository.findAll();
        assertThat(nodeCommodityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NodeCommodity.class);
        NodeCommodity nodeCommodity1 = new NodeCommodity();
        nodeCommodity1.setId(1L);
        NodeCommodity nodeCommodity2 = new NodeCommodity();
        nodeCommodity2.setId(nodeCommodity1.getId());
        assertThat(nodeCommodity1).isEqualTo(nodeCommodity2);
        nodeCommodity2.setId(2L);
        assertThat(nodeCommodity1).isNotEqualTo(nodeCommodity2);
        nodeCommodity1.setId(null);
        assertThat(nodeCommodity1).isNotEqualTo(nodeCommodity2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NodeCommodityDTO.class);
        NodeCommodityDTO nodeCommodityDTO1 = new NodeCommodityDTO();
        nodeCommodityDTO1.setId(1L);
        NodeCommodityDTO nodeCommodityDTO2 = new NodeCommodityDTO();
        assertThat(nodeCommodityDTO1).isNotEqualTo(nodeCommodityDTO2);
        nodeCommodityDTO2.setId(nodeCommodityDTO1.getId());
        assertThat(nodeCommodityDTO1).isEqualTo(nodeCommodityDTO2);
        nodeCommodityDTO2.setId(2L);
        assertThat(nodeCommodityDTO1).isNotEqualTo(nodeCommodityDTO2);
        nodeCommodityDTO1.setId(null);
        assertThat(nodeCommodityDTO1).isNotEqualTo(nodeCommodityDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(nodeCommodityMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(nodeCommodityMapper.fromId(null)).isNull();
    }
}
