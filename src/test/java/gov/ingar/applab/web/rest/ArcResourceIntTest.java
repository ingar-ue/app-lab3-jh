package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLabApp;

import gov.ingar.applab.domain.Arc;
import gov.ingar.applab.repository.ArcRepository;
import gov.ingar.applab.service.ArcService;
import gov.ingar.applab.service.dto.ArcDTO;
import gov.ingar.applab.service.mapper.ArcMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ArcResource REST controller.
 *
 * @see ArcResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLabApp.class)
public class ArcResourceIntTest {

    private static final Integer DEFAULT_CAPACITY = 1;
    private static final Integer UPDATED_CAPACITY = 2;

    private static final Boolean DEFAULT_ATTACKABLE = false;
    private static final Boolean UPDATED_ATTACKABLE = true;

    @Autowired
    private ArcRepository arcRepository;

    @Autowired
    private ArcMapper arcMapper;

    @Autowired
    private ArcService arcService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restArcMockMvc;

    private Arc arc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ArcResource arcResource = new ArcResource(arcService);
        this.restArcMockMvc = MockMvcBuilders.standaloneSetup(arcResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Arc createEntity(EntityManager em) {
        Arc arc = new Arc()
            .capacity(DEFAULT_CAPACITY)
            .attackable(DEFAULT_ATTACKABLE);
        return arc;
    }

    @Before
    public void initTest() {
        arc = createEntity(em);
    }

    @Test
    @Transactional
    public void createArc() throws Exception {
        int databaseSizeBeforeCreate = arcRepository.findAll().size();

        // Create the Arc
        ArcDTO arcDTO = arcMapper.toDto(arc);
        restArcMockMvc.perform(post("/api/arcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcDTO)))
            .andExpect(status().isCreated());

        // Validate the Arc in the database
        List<Arc> arcList = arcRepository.findAll();
        assertThat(arcList).hasSize(databaseSizeBeforeCreate + 1);
        Arc testArc = arcList.get(arcList.size() - 1);
        assertThat(testArc.getCapacity()).isEqualTo(DEFAULT_CAPACITY);
        assertThat(testArc.isAttackable()).isEqualTo(DEFAULT_ATTACKABLE);
    }

    @Test
    @Transactional
    public void createArcWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = arcRepository.findAll().size();

        // Create the Arc with an existing ID
        arc.setId(1L);
        ArcDTO arcDTO = arcMapper.toDto(arc);

        // An entity with an existing ID cannot be created, so this API call must fail
        restArcMockMvc.perform(post("/api/arcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Arc in the database
        List<Arc> arcList = arcRepository.findAll();
        assertThat(arcList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCapacityIsRequired() throws Exception {
        int databaseSizeBeforeTest = arcRepository.findAll().size();
        // set the field null
        arc.setCapacity(null);

        // Create the Arc, which fails.
        ArcDTO arcDTO = arcMapper.toDto(arc);

        restArcMockMvc.perform(post("/api/arcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcDTO)))
            .andExpect(status().isBadRequest());

        List<Arc> arcList = arcRepository.findAll();
        assertThat(arcList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAttackableIsRequired() throws Exception {
        int databaseSizeBeforeTest = arcRepository.findAll().size();
        // set the field null
        arc.setAttackable(null);

        // Create the Arc, which fails.
        ArcDTO arcDTO = arcMapper.toDto(arc);

        restArcMockMvc.perform(post("/api/arcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcDTO)))
            .andExpect(status().isBadRequest());

        List<Arc> arcList = arcRepository.findAll();
        assertThat(arcList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllArcs() throws Exception {
        // Initialize the database
        arcRepository.saveAndFlush(arc);

        // Get all the arcList
        restArcMockMvc.perform(get("/api/arcs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(arc.getId().intValue())))
            .andExpect(jsonPath("$.[*].capacity").value(hasItem(DEFAULT_CAPACITY)))
            .andExpect(jsonPath("$.[*].attackable").value(hasItem(DEFAULT_ATTACKABLE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getArc() throws Exception {
        // Initialize the database
        arcRepository.saveAndFlush(arc);

        // Get the arc
        restArcMockMvc.perform(get("/api/arcs/{id}", arc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(arc.getId().intValue()))
            .andExpect(jsonPath("$.capacity").value(DEFAULT_CAPACITY))
            .andExpect(jsonPath("$.attackable").value(DEFAULT_ATTACKABLE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingArc() throws Exception {
        // Get the arc
        restArcMockMvc.perform(get("/api/arcs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateArc() throws Exception {
        // Initialize the database
        arcRepository.saveAndFlush(arc);

        int databaseSizeBeforeUpdate = arcRepository.findAll().size();

        // Update the arc
        Arc updatedArc = arcRepository.findById(arc.getId()).get();
        // Disconnect from session so that the updates on updatedArc are not directly saved in db
        em.detach(updatedArc);
        updatedArc
            .capacity(UPDATED_CAPACITY)
            .attackable(UPDATED_ATTACKABLE);
        ArcDTO arcDTO = arcMapper.toDto(updatedArc);

        restArcMockMvc.perform(put("/api/arcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcDTO)))
            .andExpect(status().isOk());

        // Validate the Arc in the database
        List<Arc> arcList = arcRepository.findAll();
        assertThat(arcList).hasSize(databaseSizeBeforeUpdate);
        Arc testArc = arcList.get(arcList.size() - 1);
        assertThat(testArc.getCapacity()).isEqualTo(UPDATED_CAPACITY);
        assertThat(testArc.isAttackable()).isEqualTo(UPDATED_ATTACKABLE);
    }

    @Test
    @Transactional
    public void updateNonExistingArc() throws Exception {
        int databaseSizeBeforeUpdate = arcRepository.findAll().size();

        // Create the Arc
        ArcDTO arcDTO = arcMapper.toDto(arc);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restArcMockMvc.perform(put("/api/arcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arcDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Arc in the database
        List<Arc> arcList = arcRepository.findAll();
        assertThat(arcList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteArc() throws Exception {
        // Initialize the database
        arcRepository.saveAndFlush(arc);

        int databaseSizeBeforeDelete = arcRepository.findAll().size();

        // Get the arc
        restArcMockMvc.perform(delete("/api/arcs/{id}", arc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Arc> arcList = arcRepository.findAll();
        assertThat(arcList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Arc.class);
        Arc arc1 = new Arc();
        arc1.setId(1L);
        Arc arc2 = new Arc();
        arc2.setId(arc1.getId());
        assertThat(arc1).isEqualTo(arc2);
        arc2.setId(2L);
        assertThat(arc1).isNotEqualTo(arc2);
        arc1.setId(null);
        assertThat(arc1).isNotEqualTo(arc2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ArcDTO.class);
        ArcDTO arcDTO1 = new ArcDTO();
        arcDTO1.setId(1L);
        ArcDTO arcDTO2 = new ArcDTO();
        assertThat(arcDTO1).isNotEqualTo(arcDTO2);
        arcDTO2.setId(arcDTO1.getId());
        assertThat(arcDTO1).isEqualTo(arcDTO2);
        arcDTO2.setId(2L);
        assertThat(arcDTO1).isNotEqualTo(arcDTO2);
        arcDTO1.setId(null);
        assertThat(arcDTO1).isNotEqualTo(arcDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(arcMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(arcMapper.fromId(null)).isNull();
    }
}
