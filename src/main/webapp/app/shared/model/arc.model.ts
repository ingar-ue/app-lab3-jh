import { ICommodity } from 'app/shared/model/commodity.model';

export interface IArc {
    id?: number;
    capacity?: number;
    attackable?: boolean;
    node1Id?: number;
    node2Id?: number;
    node1Name?: string;
    node2Name?: string;
    // usado sólo en el resultado
    flow?: IFlow[];
    interdicted?: boolean;
}

export class Arc implements IArc {
    constructor(
        public id?: number,
        public capacity?: number,
        public attackable?: boolean,
        public node1Id?: number,
        public node2Id?: number,
        public node1Name?: string,
        public node2Name?: string
    ) {
        this.attackable = this.attackable || false;
    }
}

export interface IFlow {
    commodity?: ICommodity;
    quantity?: number;
}

export class Flow implements IFlow {
    constructor(public commodity?: ICommodity, public quantity?: number) {}

    setCommodity(commodityName: string, commodities: ICommodity[]) {
        for (const com of commodities) {
            if (com.name === commodityName) {
                this.commodity = com;
                break;
            }
        }
    }
}
