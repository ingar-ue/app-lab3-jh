export interface INodeCommodity {
    id?: number;
    supplyDemand?: number;
    commodityId?: number;
    nodeId?: number;
    commodityName?: string;
    nodeName?: string;
}

export class NodeCommodity implements INodeCommodity {
    constructor(
        public id?: number,
        public supplyDemand?: number,
        public commodityId?: number,
        public nodeId?: number,
        public commodityName?: string,
        public nodeName?: string
    ) {}
}
