import { ICommodity } from 'app/shared/model/commodity.model';

export interface INode {
    id?: number;
    name?: string;
    // usado sólo en el resultado
    remainingSupply?: IRemaining;
    remainingDemand?: IRemaining;
}

export class Node implements INode {
    constructor(public id?: number, public name?: string) {
        this.name = name;
    }
}

export interface IRemaining {
    commodity?: ICommodity;
    remaining?: number;
}

export class Remaining implements IRemaining {
    constructor(public commodity?: ICommodity, public remaining?: number) {}

    setCommodity(commodityName: string, commodities: ICommodity[]) {
        for (const com of commodities) {
            if (com.name === commodityName) {
                this.commodity = com;
                break;
            }
        }
    }
}
