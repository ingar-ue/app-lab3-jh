import { IModelRunDef, ModelRunDef } from 'app/shared/model/model-run-def.model';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';
import { INode } from 'app/shared/model/node.model';
import { IArc } from 'app/shared/model/arc.model';
import { ICommodity } from 'app/shared/model/commodity.model';
import { IArcCommodity } from 'app/shared/model/arc-commodity.model';
import { INodeCommodity } from 'app/shared/model/node-commodity.model';

export interface IOptimizationPoint1 {
    nodes?: INode[];
    arcs?: IArc[];
    commodities?: ICommodity[];
    arcCommodities?: IArcCommodity[];
    nodeCommodities?: INodeCommodity[];
    modelRunDef?: IModelRunDef;
    modelDefId?: number;
    modelRunConfigs?: IModelRunConfig[];
}

export class OptimizationPoint1 implements IOptimizationPoint1 {
    constructor(
        public nodes?: INode[],
        public arcs?: IArc[],
        public commodities?: ICommodity[],
        public arcCommodities?: IArcCommodity[],
        public nodeCommodities?: INodeCommodity[],
        public modelRunDef?: ModelRunDef,
        public modelDefId?: number,
        public modelRunConfigs?: IModelRunConfig[]
    ) {}
}

export interface IResult {
    total?: number;
    nodes?: INode[];
    arcs?: IArc[];
}
