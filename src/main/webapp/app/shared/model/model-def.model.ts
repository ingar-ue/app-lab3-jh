import { IModelConfig, ModelConfig } from 'app/shared/model/model-config.model';
import { IModelVersion, ModelVersion } from 'app/shared/model/model-version.model';
import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';

export const enum Language {
    GAMS = 'GAMS',
    PYOMO = 'PYOMO',
    ORTOOLS = 'ORTOOLS'
}

export const enum Solver {
    CPLEX = 'CPLEX',
    GUROBI = 'GUROBI',
    GLPK = 'GLPK',
    CBC = 'CBC'
}

export interface IModelDef {
    id?: number;
    name?: string;
    language?: Language;
    solver?: Solver;
    currentVersion?: number;
    description?: string;
    modelConfigs?: IModelConfig[];
    modelVersions?: IModelVersion[];
    modelObjFunctions?: IModelObjFunction[];
}

export class ModelDef implements IModelDef {
    constructor(
        public id?: number,
        public name?: string,
        public language?: Language,
        public solver?: Solver,
        public currentVersion?: number,
        public description?: string,
        public modelConfigs?: ModelConfig[],
        public modelVersions?: ModelVersion[],
        public modelObjFunctions?: IModelObjFunction[]
    ) {}
}
