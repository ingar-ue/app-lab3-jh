export interface IArcCommodity {
    id?: number;
    cost?: number;
    capacity?: number;
    commodityId?: number;
    arcId?: number;
    arcName?: string;
    arcNode1Name?: string;
    arcNode2Name?: string;
    commodityName?: string;
}

export class ArcCommodity implements IArcCommodity {
    constructor(
        public id?: number,
        public cost?: number,
        public capacity?: number,
        public commodityId?: number,
        public arcId?: number,
        public arcName?: string,
        public arcNode1Name?: string,
        public arcNode2Name?: string,
        public commodityName?: string
    ) {}
}
