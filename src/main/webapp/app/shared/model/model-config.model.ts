export interface IModelConfig {
    id?: number;
    param?: string;
    value?: string;
    description?: string;
    modelDefId?: number;
}

export class ModelConfig implements IModelConfig {
    constructor(
        public id?: number,
        public param?: string,
        public value?: string,
        public description?: string,
        public modelDefId?: number
    ) {}
}
