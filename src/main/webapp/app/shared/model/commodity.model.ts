export interface ICommodity {
    id?: number;
    name?: string;
}

export class Commodity implements ICommodity {
    constructor(public id?: number, public name?: string) {}
}
