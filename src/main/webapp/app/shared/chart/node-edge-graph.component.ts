import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { Component, Input, OnChanges } from '@angular/core';
import { IOptimizationPoint1 } from 'app/shared/model/optimization-point-1.model';
import * as d3 from 'D3';

@Component({
    selector: 'jhi-node-edge-graph',
    templateUrl: './node-edge-graph.component.html'
})
export class NodeEdgeGraphComponent implements OnChanges {
    chartsLoaded = false;
    @Input()
    modelRunDef: IModelRunDef;

    svg;
    width = 650;
    height = 400;
    radius = 15;
    nodes;
    links;
    simulation;
    graph_data;

    constructor() {}

    ngOnChanges() {
        if (!this.chartsLoaded && this.modelRunDef.status === 'FINISHED') {
            this.graph_data = this.graphData();
            console.log(this.graph_data);
            this.makeGraph();
            this.chartsLoaded = true;
        }
    }

    graphData(result = this.modelRunDef.userResult): any {
        const arcs = [];
        for (const arc of result.arcs) {
            arcs.push({ source: arc.node1Name, target: arc.node2Name, interdicted: arc.interdicted, flow: arc.flow });
        }
        return { nodes: result.nodes, links: arcs };
    }

    makeGraph() {
        this.svg = d3
            .select('jhi-node-edge-graph')
            .append('svg')
            .attr('width', this.width)
            .attr('height', this.height);

        this.svg
            .append('defs')
            .append('marker')
            .attr('id', 'arrowhead')
            .attr('viewBox', '-0 -5 10 10')
            .attr('refX', 22)
            .attr('refY', 0)
            .attr('orient', 'auto')
            .attr('markerWidth', 6)
            .attr('markerHeight', 6)
            .attr('xoverflow', 'visible')
            .append('svg:path')
            .attr('d', 'M 0,-5 L 10 ,0 L 0,5')
            .attr('fill', '#999')
            .style('stroke', 'none');

        this.links = this.svg
            .append('g')
            .attr('class', 'links')
            .selectAll('line')
            .data(this.graph_data.links)
            .enter()
            .append('line')
            .attr('stroke-width', 2)
            .style('stroke', function(d) {
                if (d.interdicted) {
                    return 'red';
                } else {
                    return '#999';
                }
            })
            .attr('marker-end', 'url(#arrowhead)');

        this.nodes = this.svg
            .selectAll('.node')
            .data(this.graph_data.nodes)
            .enter()
            .append('g')
            .attr('class', 'node');

        this.nodes
            .append('circle')
            .attr('r', this.radius)
            .attr('fill', '#007bff');

        this.nodes
            .append('text')
            .style('fill', '#212529')
            .style('font-weight', 'bold')
            .attr('dy', '.35em')
            .attr('text-anchor', 'middle')
            .text(function(d) {
                return d.name;
            });

        this.forceSimulationSetup();
        this.dragSetup();
    }

    private forceSimulationSetup() {
        const _this = this;
        this.simulation = d3
            .forceSimulation()
            .nodes(this.graph_data.nodes)
            .force('charge_force', d3.forceManyBody().strength(-100))
            .force('center_force', d3.forceCenter(this.width / 2, this.height / 2))
            .force('box_force', box_force());
        this.simulation.on('tick', tickActions);

        const link_force = d3
            .forceLink(this.graph_data.links)
            .id(function(d) {
                return d.name;
            })
            .distance(150);
        this.simulation.force('links', link_force);

        function tickActions() {
            _this.nodes.attr('transform', function(d) {
                return 'translate(' + d.x + ',' + d.y + ')';
            });

            _this.links
                .attr('x1', function(d) {
                    return d.source.x;
                })
                .attr('y1', function(d) {
                    return d.source.y;
                })
                .attr('x2', function(d) {
                    return d.target.x;
                })
                .attr('y2', function(d) {
                    return d.target.y;
                });
        }

        function box_force() {
            for (let i = 0, n = _this.graph_data.nodes.length; i < n; ++i) {
                const curr_node = _this.graph_data.nodes[i];
                curr_node.x = Math.max(_this.radius, Math.min(_this.width - _this.radius, curr_node.x));
                curr_node.y = Math.max(_this.radius, Math.min(_this.height - _this.radius, curr_node.y));
            }
        }
    }

    private dragSetup() {
        const _this = this;
        const drag_handler = d3
            .drag()
            .on('start', drag_start)
            .on('drag', drag_drag)
            .on('end', drag_end);
        drag_handler(this.nodes);

        function drag_start(d) {
            if (!d3.event.active) {
                _this.simulation.alphaTarget(0.3).restart();
            }
            d.fx = d.x;
            d.fy = d.y;
        }

        function drag_drag(d) {
            d.fx = Math.max(_this.radius, Math.min(_this.width - _this.radius, d3.event.x));
            d.fy = Math.max(_this.radius, Math.min(_this.height - _this.radius, d3.event.y));
        }

        function drag_end(d) {
            if (!d3.event.active) {
                _this.simulation.alphaTarget(0);
            }
            d.fx = null;
            d.fy = null;
        }
    }
}
