import { Component, Input, OnChanges } from '@angular/core';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { IOptimizationPoint1 } from 'app/shared/model/optimization-point-1.model';

@Component({
    selector: 'jhi-optimization-point-1-charts',
    templateUrl: './optimization-point-1-charts.component.html'
})
export class OptimizationPoint1ChartsComponent implements OnChanges {
    @Input()
    change: number;
    @Input()
    modelRunDef: IModelRunDef;

    charts: any;
    chartsLoaded = false;
    currentChart: number;

    constructor() {
        this.currentChart = 1;
    }

    ngOnChanges() {}
}
