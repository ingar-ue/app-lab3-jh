import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { AppLabSharedLibsModule, AppLabSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';
import { StringFilterPipe } from 'app/shared/pipes/string-filter.pipe';
import { OptimizationPoint1ChartsComponent } from 'app/shared/chart/optimization-point-1-charts.component';
import { ChartModule } from 'angular2-chartjs';
import { SortPipe } from 'app/shared/pipes/sort.pipe';
import { CustomFilterPipe } from 'app/shared/pipes/custom-filter.pipe';
import { NodeEdgeGraphComponent } from 'app/shared/chart/node-edge-graph.component';
@NgModule({
    imports: [AppLabSharedLibsModule, AppLabSharedCommonModule, ChartModule],
    declarations: [
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        StringFilterPipe,
        OptimizationPoint1ChartsComponent,
        NodeEdgeGraphComponent,
        SortPipe,
        CustomFilterPipe
    ],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }, OptimizationPoint1ChartsComponent, NodeEdgeGraphComponent],
    entryComponents: [JhiLoginModalComponent, NodeEdgeGraphComponent, OptimizationPoint1ChartsComponent],
    exports: [
        AppLabSharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        StringFilterPipe,
        OptimizationPoint1ChartsComponent,
        NodeEdgeGraphComponent,
        SortPipe,
        CustomFilterPipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabSharedModule {}
