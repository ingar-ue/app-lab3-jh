import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppLabNodeModule } from './node/node.module';
import { AppLabArcModule } from './arc/arc.module';
import { AppLabCommodityModule } from './commodity/commodity.module';
import { AppLabNodeCommodityModule } from './node-commodity/node-commodity.module';
import { AppLabArcCommodityModule } from './arc-commodity/arc-commodity.module';
import { AppLabEnvConfigModule } from './env-config/env-config.module';
import { AppLabModelDefModule } from './model-def/model-def.module';
import { AppLabModelConfigModule } from './model-config/model-config.module';
import { AppLabModelVersionModule } from './model-version/model-version.module';
import { AppLabModelObjFunctionModule } from './model-obj-function/model-obj-function.module';
import { AppLabModelRunDefModule } from './model-run-def/model-run-def.module';
import { AppLabOptimizationPoint1Module } from 'app/entities/optimization-point-1/optimization-point-1.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        AppLabNodeModule,
        AppLabArcModule,
        AppLabCommodityModule,
        AppLabNodeCommodityModule,
        AppLabArcCommodityModule,
        AppLabEnvConfigModule,
        AppLabModelDefModule,
        AppLabModelConfigModule,
        AppLabModelVersionModule,
        AppLabModelObjFunctionModule,
        AppLabModelRunDefModule,
        AppLabOptimizationPoint1Module
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabEntityModule {}
