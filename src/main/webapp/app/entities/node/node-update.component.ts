import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { INode } from 'app/shared/model/node.model';
import { NodeService } from './node.service';

@Component({
    selector: 'jhi-node-update',
    templateUrl: './node-update.component.html'
})
export class NodeUpdateComponent implements OnInit {
    node: INode;
    isSaving: boolean;

    constructor(private nodeService: NodeService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ node }) => {
            this.node = node;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.node.id !== undefined) {
            this.subscribeToSaveResponse(this.nodeService.update(this.node));
        } else {
            this.subscribeToSaveResponse(this.nodeService.create(this.node));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<INode>>) {
        result.subscribe((res: HttpResponse<INode>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
