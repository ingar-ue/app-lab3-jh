import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IArc } from 'app/shared/model/arc.model';
import { ArcService } from './arc.service';
import { INode } from 'app/shared/model/node.model';
import { NodeService } from 'app/entities/node';

@Component({
    selector: 'jhi-arc-update',
    templateUrl: './arc-update.component.html'
})
export class ArcUpdateComponent implements OnInit {
    arc: IArc;
    isSaving: boolean;

    nodes: INode[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private arcService: ArcService,
        private nodeService: NodeService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ arc }) => {
            this.arc = arc;
        });
        this.nodeService.query().subscribe(
            (res: HttpResponse<INode[]>) => {
                this.nodes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.arc.id !== undefined) {
            this.subscribeToSaveResponse(this.arcService.update(this.arc));
        } else {
            this.subscribeToSaveResponse(this.arcService.create(this.arc));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IArc>>) {
        result.subscribe((res: HttpResponse<IArc>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackNodeById(index: number, item: INode) {
        return item.id;
    }
}
