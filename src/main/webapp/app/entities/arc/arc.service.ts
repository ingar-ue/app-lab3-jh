import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IArc } from 'app/shared/model/arc.model';

type EntityResponseType = HttpResponse<IArc>;
type EntityArrayResponseType = HttpResponse<IArc[]>;

@Injectable({ providedIn: 'root' })
export class ArcService {
    public resourceUrl = SERVER_API_URL + 'api/arcs';

    constructor(private http: HttpClient) {}

    create(arc: IArc): Observable<EntityResponseType> {
        return this.http.post<IArc>(this.resourceUrl, arc, { observe: 'response' });
    }

    update(arc: IArc): Observable<EntityResponseType> {
        return this.http.put<IArc>(this.resourceUrl, arc, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IArc>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IArc[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
