import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Arc } from 'app/shared/model/arc.model';
import { ArcService } from './arc.service';
import { ArcComponent } from './arc.component';
import { ArcDetailComponent } from './arc-detail.component';
import { ArcUpdateComponent } from './arc-update.component';
import { ArcDeletePopupComponent } from './arc-delete-dialog.component';
import { IArc } from 'app/shared/model/arc.model';

@Injectable({ providedIn: 'root' })
export class ArcResolve implements Resolve<IArc> {
    constructor(private service: ArcService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Arc> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Arc>) => response.ok),
                map((arc: HttpResponse<Arc>) => arc.body)
            );
        }
        return of(new Arc());
    }
}

export const arcRoute: Routes = [
    {
        path: 'arc',
        component: ArcComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.arc.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'arc/:id/view',
        component: ArcDetailComponent,
        resolve: {
            arc: ArcResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.arc.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'arc/new',
        component: ArcUpdateComponent,
        resolve: {
            arc: ArcResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.arc.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'arc/:id/edit',
        component: ArcUpdateComponent,
        resolve: {
            arc: ArcResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.arc.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const arcPopupRoute: Routes = [
    {
        path: 'arc/:id/delete',
        component: ArcDeletePopupComponent,
        resolve: {
            arc: ArcResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.arc.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
