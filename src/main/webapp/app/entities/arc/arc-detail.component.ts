import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IArc } from 'app/shared/model/arc.model';

@Component({
    selector: 'jhi-arc-detail',
    templateUrl: './arc-detail.component.html'
})
export class ArcDetailComponent implements OnInit {
    arc: IArc;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ arc }) => {
            this.arc = arc;
        });
    }

    previousState() {
        window.history.back();
    }
}
