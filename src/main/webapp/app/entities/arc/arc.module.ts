import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ArcComponent,
    ArcDetailComponent,
    ArcUpdateComponent,
    ArcDeletePopupComponent,
    ArcDeleteDialogComponent,
    arcRoute,
    arcPopupRoute
} from './';

const ENTITY_STATES = [...arcRoute, ...arcPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [ArcComponent, ArcDetailComponent, ArcUpdateComponent, ArcDeleteDialogComponent, ArcDeletePopupComponent],
    entryComponents: [ArcComponent, ArcUpdateComponent, ArcDeleteDialogComponent, ArcDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabArcModule {}
