import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IArcCommodity } from 'app/shared/model/arc-commodity.model';

@Component({
    selector: 'jhi-arc-commodity-detail',
    templateUrl: './arc-commodity-detail.component.html'
})
export class ArcCommodityDetailComponent implements OnInit {
    arcCommodity: IArcCommodity;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ arcCommodity }) => {
            this.arcCommodity = arcCommodity;
        });
    }

    previousState() {
        window.history.back();
    }
}
