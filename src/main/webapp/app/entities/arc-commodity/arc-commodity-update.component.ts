import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IArcCommodity } from 'app/shared/model/arc-commodity.model';
import { ArcCommodityService } from './arc-commodity.service';
import { ICommodity } from 'app/shared/model/commodity.model';
import { CommodityService } from 'app/entities/commodity';
import { IArc } from 'app/shared/model/arc.model';
import { ArcService } from 'app/entities/arc';

@Component({
    selector: 'jhi-arc-commodity-update',
    templateUrl: './arc-commodity-update.component.html'
})
export class ArcCommodityUpdateComponent implements OnInit {
    arcCommodity: IArcCommodity;
    isSaving: boolean;

    commodities: ICommodity[];

    arcs: IArc[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private arcCommodityService: ArcCommodityService,
        private commodityService: CommodityService,
        private arcService: ArcService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ arcCommodity }) => {
            this.arcCommodity = arcCommodity;
        });
        this.commodityService.query().subscribe(
            (res: HttpResponse<ICommodity[]>) => {
                this.commodities = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.arcService.query().subscribe(
            (res: HttpResponse<IArc[]>) => {
                this.arcs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.arcCommodity.id !== undefined) {
            this.subscribeToSaveResponse(this.arcCommodityService.update(this.arcCommodity));
        } else {
            this.subscribeToSaveResponse(this.arcCommodityService.create(this.arcCommodity));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IArcCommodity>>) {
        result.subscribe((res: HttpResponse<IArcCommodity>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCommodityById(index: number, item: ICommodity) {
        return item.id;
    }

    trackArcById(index: number, item: IArc) {
        return item.id;
    }
}
