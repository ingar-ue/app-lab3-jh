import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IArcCommodity } from 'app/shared/model/arc-commodity.model';
import { ArcCommodityService } from './arc-commodity.service';

@Component({
    selector: 'jhi-arc-commodity-delete-dialog',
    templateUrl: './arc-commodity-delete-dialog.component.html'
})
export class ArcCommodityDeleteDialogComponent {
    arcCommodity: IArcCommodity;

    constructor(
        private arcCommodityService: ArcCommodityService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.arcCommodityService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'arcCommodityListModification',
                content: 'Deleted an arcCommodity'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-arc-commodity-delete-popup',
    template: ''
})
export class ArcCommodityDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ arcCommodity }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ArcCommodityDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.arcCommodity = arcCommodity;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
