import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ArcCommodity } from 'app/shared/model/arc-commodity.model';
import { ArcCommodityService } from './arc-commodity.service';
import { ArcCommodityComponent } from './arc-commodity.component';
import { ArcCommodityDetailComponent } from './arc-commodity-detail.component';
import { ArcCommodityUpdateComponent } from './arc-commodity-update.component';
import { ArcCommodityDeletePopupComponent } from './arc-commodity-delete-dialog.component';
import { IArcCommodity } from 'app/shared/model/arc-commodity.model';

@Injectable({ providedIn: 'root' })
export class ArcCommodityResolve implements Resolve<IArcCommodity> {
    constructor(private service: ArcCommodityService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ArcCommodity> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<ArcCommodity>) => response.ok),
                map((arcCommodity: HttpResponse<ArcCommodity>) => arcCommodity.body)
            );
        }
        return of(new ArcCommodity());
    }
}

export const arcCommodityRoute: Routes = [
    {
        path: 'arc-commodity',
        component: ArcCommodityComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.arcCommodity.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'arc-commodity/:id/view',
        component: ArcCommodityDetailComponent,
        resolve: {
            arcCommodity: ArcCommodityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.arcCommodity.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'arc-commodity/new',
        component: ArcCommodityUpdateComponent,
        resolve: {
            arcCommodity: ArcCommodityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.arcCommodity.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'arc-commodity/:id/edit',
        component: ArcCommodityUpdateComponent,
        resolve: {
            arcCommodity: ArcCommodityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.arcCommodity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const arcCommodityPopupRoute: Routes = [
    {
        path: 'arc-commodity/:id/delete',
        component: ArcCommodityDeletePopupComponent,
        resolve: {
            arcCommodity: ArcCommodityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.arcCommodity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
