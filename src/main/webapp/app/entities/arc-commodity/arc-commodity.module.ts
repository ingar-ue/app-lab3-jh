import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ArcCommodityComponent,
    ArcCommodityDetailComponent,
    ArcCommodityUpdateComponent,
    ArcCommodityDeletePopupComponent,
    ArcCommodityDeleteDialogComponent,
    arcCommodityRoute,
    arcCommodityPopupRoute
} from './';

const ENTITY_STATES = [...arcCommodityRoute, ...arcCommodityPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ArcCommodityComponent,
        ArcCommodityDetailComponent,
        ArcCommodityUpdateComponent,
        ArcCommodityDeleteDialogComponent,
        ArcCommodityDeletePopupComponent
    ],
    entryComponents: [
        ArcCommodityComponent,
        ArcCommodityUpdateComponent,
        ArcCommodityDeleteDialogComponent,
        ArcCommodityDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabArcCommodityModule {}
