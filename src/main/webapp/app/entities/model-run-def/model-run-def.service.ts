import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';

type EntityResponseType = HttpResponse<IModelRunDef>;
type EntityArrayResponseType = HttpResponse<IModelRunDef[]>;

@Injectable({ providedIn: 'root' })
export class ModelRunDefService {
    private resourceUrl = SERVER_API_URL + 'api/model-run-defs';

    constructor(private http: HttpClient) {}

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IModelRunDef>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IModelRunDef[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(modelRunDef: IModelRunDef): IModelRunDef {
        const copy: IModelRunDef = Object.assign({}, modelRunDef, {
            runDate: modelRunDef.runDate != null && modelRunDef.runDate.isValid() ? modelRunDef.runDate.format(DATE_FORMAT) : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.runDate = res.body.runDate != null ? moment(res.body.runDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((modelRunDef: IModelRunDef) => {
            modelRunDef.runDate = modelRunDef.runDate != null ? moment(modelRunDef.runDate) : null;
        });
        return res;
    }
}
