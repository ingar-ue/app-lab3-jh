import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IModelDef } from 'app/shared/model/model-def.model';
import { ModelDefService } from './model-def.service';

@Component({
    selector: 'jhi-model-def-delete-dialog',
    templateUrl: './model-def-delete-dialog.component.html'
})
export class ModelDefDeleteDialogComponent {
    modelDef: IModelDef;

    constructor(private modelDefService: ModelDefService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.modelDefService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'modelDefListModification',
                content: 'Deleted an modelDef'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-model-def-delete-popup',
    template: ''
})
export class ModelDefDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelDef }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ModelDefDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.modelDef = modelDef;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
