import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IModelDef } from 'app/shared/model/model-def.model';
import { ModelDefService } from './model-def.service';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

@Component({
    selector: 'jhi-model-def-update',
    templateUrl: './model-def-update.component.html'
})
export class ModelDefUpdateDialogComponent implements OnInit {
    modelDef: IModelDef;
    isSaving: boolean;

    constructor(private modelDefService: ModelDefService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.modelDef.id !== undefined) {
            this.subscribeToSaveResponse(this.modelDefService.update(this.modelDef));
        } else {
            this.subscribeToSaveResponse(this.modelDefService.create(this.modelDef));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IModelDef>>) {
        result.subscribe((res: HttpResponse<IModelDef>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.eventManager.broadcast({
            name: 'modelDefListModification',
            content: 'Edited a modelConfig'
        });
        this.activeModal.dismiss(true);
    }

    private onSaveError() {
        this.isSaving = false;
        this.activeModal.dismiss(true);
    }
}

@Component({
    selector: 'jhi-model-def-update-popup',
    template: ''
})
export class ModelDefUpdatePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelDef }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ModelDefUpdateDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.modelDef = modelDef;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
