import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ModelDefComponent,
    ModelDefDetailComponent,
    ModelDefUpdateDialogComponent,
    ModelDefUpdatePopupComponent,
    ModelDefDeletePopupComponent,
    ModelDefDeleteDialogComponent,
    modelDefRoute,
    modelDefPopupRoute
} from './';

const ENTITY_STATES = [...modelDefRoute, ...modelDefPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelDefComponent,
        ModelDefDetailComponent,
        ModelDefUpdateDialogComponent,
        ModelDefUpdatePopupComponent,
        ModelDefDeleteDialogComponent,
        ModelDefDeletePopupComponent
    ],
    entryComponents: [
        ModelDefComponent,
        ModelDefUpdateDialogComponent,
        ModelDefUpdatePopupComponent,
        ModelDefDeleteDialogComponent,
        ModelDefDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabModelDefModule {}
