import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { EnvConfig } from 'app/shared/model/env-config.model';
import { EnvConfigService } from './env-config.service';
import { EnvConfigComponent } from './env-config.component';
import { EnvConfigDetailComponent } from './env-config-detail.component';
import { EnvConfigUpdateComponent } from './env-config-update.component';
import { EnvConfigDeletePopupComponent } from './env-config-delete-dialog.component';
import { IEnvConfig } from 'app/shared/model/env-config.model';

@Injectable({ providedIn: 'root' })
export class EnvConfigResolve implements Resolve<IEnvConfig> {
    constructor(private service: EnvConfigService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<EnvConfig> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<EnvConfig>) => response.ok),
                map((envConfig: HttpResponse<EnvConfig>) => envConfig.body)
            );
        }
        return of(new EnvConfig());
    }
}

export const envConfigRoute: Routes = [
    {
        path: 'env-config',
        component: EnvConfigComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.envConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'env-config/:id/view',
        component: EnvConfigDetailComponent,
        resolve: {
            envConfig: EnvConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.envConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'env-config/new',
        component: EnvConfigUpdateComponent,
        resolve: {
            envConfig: EnvConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.envConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'env-config/:id/edit',
        component: EnvConfigUpdateComponent,
        resolve: {
            envConfig: EnvConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.envConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const envConfigPopupRoute: Routes = [
    {
        path: 'env-config/:id/delete',
        component: EnvConfigDeletePopupComponent,
        resolve: {
            envConfig: EnvConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.envConfig.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
