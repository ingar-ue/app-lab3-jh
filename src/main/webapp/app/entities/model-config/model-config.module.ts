import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ModelConfigUpdateDialogComponent,
    ModelConfigUpdatePopupComponent,
    ModelConfigDeletePopupComponent,
    ModelConfigDeleteDialogComponent,
    modelConfigRoute,
    modelConfigPopupRoute
} from './';

const ENTITY_STATES = [...modelConfigRoute, ...modelConfigPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelConfigUpdateDialogComponent,
        ModelConfigUpdatePopupComponent,
        ModelConfigDeleteDialogComponent,
        ModelConfigDeletePopupComponent
    ],
    entryComponents: [
        ModelConfigUpdateDialogComponent,
        ModelConfigUpdatePopupComponent,
        ModelConfigDeleteDialogComponent,
        ModelConfigDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabModelConfigModule {}
