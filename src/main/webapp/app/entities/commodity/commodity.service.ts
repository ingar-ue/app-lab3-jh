import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICommodity } from 'app/shared/model/commodity.model';

type EntityResponseType = HttpResponse<ICommodity>;
type EntityArrayResponseType = HttpResponse<ICommodity[]>;

@Injectable({ providedIn: 'root' })
export class CommodityService {
    public resourceUrl = SERVER_API_URL + 'api/commodities';

    constructor(private http: HttpClient) {}

    create(commodity: ICommodity): Observable<EntityResponseType> {
        return this.http.post<ICommodity>(this.resourceUrl, commodity, { observe: 'response' });
    }

    update(commodity: ICommodity): Observable<EntityResponseType> {
        return this.http.put<ICommodity>(this.resourceUrl, commodity, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICommodity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICommodity[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
