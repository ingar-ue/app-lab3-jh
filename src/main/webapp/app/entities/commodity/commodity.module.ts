import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    CommodityComponent,
    CommodityDetailComponent,
    CommodityUpdateComponent,
    CommodityDeletePopupComponent,
    CommodityDeleteDialogComponent,
    commodityRoute,
    commodityPopupRoute
} from './';

const ENTITY_STATES = [...commodityRoute, ...commodityPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CommodityComponent,
        CommodityDetailComponent,
        CommodityUpdateComponent,
        CommodityDeleteDialogComponent,
        CommodityDeletePopupComponent
    ],
    entryComponents: [CommodityComponent, CommodityUpdateComponent, CommodityDeleteDialogComponent, CommodityDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabCommodityModule {}
