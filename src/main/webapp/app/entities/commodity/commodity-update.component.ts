import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICommodity } from 'app/shared/model/commodity.model';
import { CommodityService } from './commodity.service';

@Component({
    selector: 'jhi-commodity-update',
    templateUrl: './commodity-update.component.html'
})
export class CommodityUpdateComponent implements OnInit {
    commodity: ICommodity;
    isSaving: boolean;

    constructor(private commodityService: CommodityService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ commodity }) => {
            this.commodity = commodity;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.commodity.id !== undefined) {
            this.subscribeToSaveResponse(this.commodityService.update(this.commodity));
        } else {
            this.subscribeToSaveResponse(this.commodityService.create(this.commodity));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICommodity>>) {
        result.subscribe((res: HttpResponse<ICommodity>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
