import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    ModelObjFunctionUpdateDialogComponent,
    ModelObjFunctionUpdatePopupComponent,
    ModelObjFunctionDeletePopupComponent,
    ModelObjFunctionDeleteDialogComponent,
    modelObjFunctionRoute,
    modelObjFunctionPopupRoute
} from './';

const ENTITY_STATES = [...modelObjFunctionRoute, ...modelObjFunctionPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelObjFunctionUpdateDialogComponent,
        ModelObjFunctionUpdatePopupComponent,
        ModelObjFunctionDeleteDialogComponent,
        ModelObjFunctionDeletePopupComponent
    ],
    entryComponents: [
        ModelObjFunctionUpdateDialogComponent,
        ModelObjFunctionUpdatePopupComponent,
        ModelObjFunctionDeleteDialogComponent,
        ModelObjFunctionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabModelObjFunctionModule {}
