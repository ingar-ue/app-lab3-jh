import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';

type EntityResponseType = HttpResponse<IModelObjFunction>;
type EntityArrayResponseType = HttpResponse<IModelObjFunction[]>;

@Injectable({ providedIn: 'root' })
export class ModelObjFunctionService {
    private resourceUrl = SERVER_API_URL + 'api/model-obj-functions';

    constructor(private http: HttpClient) {}

    create(modelObjFunction: IModelObjFunction): Observable<EntityResponseType> {
        return this.http.post<IModelObjFunction>(this.resourceUrl, modelObjFunction, { observe: 'response' });
    }

    update(modelObjFunction: IModelObjFunction): Observable<EntityResponseType> {
        return this.http.put<IModelObjFunction>(this.resourceUrl, modelObjFunction, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IModelObjFunction>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IModelObjFunction[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
