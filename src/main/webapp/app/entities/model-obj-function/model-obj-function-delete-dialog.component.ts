import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { ModelObjFunctionService } from './model-obj-function.service';

@Component({
    selector: 'jhi-model-obj-function-delete-dialog',
    templateUrl: './model-obj-function-delete-dialog.component.html'
})
export class ModelObjFunctionDeleteDialogComponent {
    modelObjFunction: IModelObjFunction;

    constructor(
        private modelObjFunctionService: ModelObjFunctionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.modelObjFunctionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'modelObjFunctionListModification',
                content: 'Deleted an modelObjFunction'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-model-obj-function-delete-popup',
    template: ''
})
export class ModelObjFunctionDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelObjFunction }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ModelObjFunctionDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.modelObjFunction = modelObjFunction;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
