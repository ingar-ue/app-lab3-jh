import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { ModelObjFunctionService } from './model-obj-function.service';
import { IModelDef } from 'app/shared/model/model-def.model';
import { ModelDefService } from 'app/entities/model-def';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModelConfigUpdateDialogComponent } from 'app/entities/model-config/model-config-update.component';

@Component({
    selector: 'jhi-model-obj-function-update',
    templateUrl: './model-obj-function-update.component.html'
})
export class ModelObjFunctionUpdateDialogComponent implements OnInit {
    modelObjFunction: IModelObjFunction;
    isSaving: boolean;

    modeldefs: IModelDef[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private modelObjFunctionService: ModelObjFunctionService,
        private modelDefService: ModelDefService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    ngOnInit() {
        this.isSaving = false;
        // this.modelDefService.query().subscribe(
        //     (res: HttpResponse<IModelDef[]>) => {
        //         this.modeldefs = res.body;
        //     },
        //     (res: HttpErrorResponse) => this.onError(res.message)
        // );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.modelObjFunction.id !== undefined) {
            this.subscribeToSaveResponse(this.modelObjFunctionService.update(this.modelObjFunction));
        } else {
            this.subscribeToSaveResponse(this.modelObjFunctionService.create(this.modelObjFunction));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IModelObjFunction>>) {
        result.subscribe((res: HttpResponse<IModelObjFunction>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.eventManager.broadcast({
            name: 'modelObjFunctionListModification',
            content: 'Edited a modelObjFunction'
        });
        this.activeModal.dismiss(true);
    }

    private onSaveError() {
        this.isSaving = false;
        this.activeModal.dismiss(true);
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackModelDefById(index: number, item: IModelDef) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-model-obj-function-update-popup',
    template: ''
})
export class ModelObjFunctionUpdatePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelObjFunction }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ModelObjFunctionUpdateDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.modelObjFunction = modelObjFunction;
                this.activatedRoute.params.subscribe(params => {
                    this.ngbModalRef.componentInstance.modelObjFunction.modelDefId = params['modelDefId'];
                });
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
