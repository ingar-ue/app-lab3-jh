import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { INodeCommodity } from 'app/shared/model/node-commodity.model';
import { NodeCommodityService } from './node-commodity.service';
import { ICommodity } from 'app/shared/model/commodity.model';
import { CommodityService } from 'app/entities/commodity';
import { INode } from 'app/shared/model/node.model';
import { NodeService } from 'app/entities/node';

@Component({
    selector: 'jhi-node-commodity-update',
    templateUrl: './node-commodity-update.component.html'
})
export class NodeCommodityUpdateComponent implements OnInit {
    nodeCommodity: INodeCommodity;
    isSaving: boolean;

    commodities: ICommodity[];

    nodes: INode[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private nodeCommodityService: NodeCommodityService,
        private commodityService: CommodityService,
        private nodeService: NodeService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ nodeCommodity }) => {
            this.nodeCommodity = nodeCommodity;
        });
        this.commodityService.query().subscribe(
            (res: HttpResponse<ICommodity[]>) => {
                this.commodities = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.nodeService.query().subscribe(
            (res: HttpResponse<INode[]>) => {
                this.nodes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.nodeCommodity.id !== undefined) {
            this.subscribeToSaveResponse(this.nodeCommodityService.update(this.nodeCommodity));
        } else {
            this.subscribeToSaveResponse(this.nodeCommodityService.create(this.nodeCommodity));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<INodeCommodity>>) {
        result.subscribe((res: HttpResponse<INodeCommodity>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCommodityById(index: number, item: ICommodity) {
        return item.id;
    }

    trackNodeById(index: number, item: INode) {
        return item.id;
    }
}
