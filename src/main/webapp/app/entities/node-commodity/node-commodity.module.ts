import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLabSharedModule } from 'app/shared';
import {
    NodeCommodityComponent,
    NodeCommodityDetailComponent,
    NodeCommodityUpdateComponent,
    NodeCommodityDeletePopupComponent,
    NodeCommodityDeleteDialogComponent,
    nodeCommodityRoute,
    nodeCommodityPopupRoute
} from './';

const ENTITY_STATES = [...nodeCommodityRoute, ...nodeCommodityPopupRoute];

@NgModule({
    imports: [AppLabSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        NodeCommodityComponent,
        NodeCommodityDetailComponent,
        NodeCommodityUpdateComponent,
        NodeCommodityDeleteDialogComponent,
        NodeCommodityDeletePopupComponent
    ],
    entryComponents: [
        NodeCommodityComponent,
        NodeCommodityUpdateComponent,
        NodeCommodityDeleteDialogComponent,
        NodeCommodityDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabNodeCommodityModule {}
