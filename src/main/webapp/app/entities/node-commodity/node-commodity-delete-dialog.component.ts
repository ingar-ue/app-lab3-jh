import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { INodeCommodity } from 'app/shared/model/node-commodity.model';
import { NodeCommodityService } from './node-commodity.service';

@Component({
    selector: 'jhi-node-commodity-delete-dialog',
    templateUrl: './node-commodity-delete-dialog.component.html'
})
export class NodeCommodityDeleteDialogComponent {
    nodeCommodity: INodeCommodity;

    constructor(
        private nodeCommodityService: NodeCommodityService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.nodeCommodityService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'nodeCommodityListModification',
                content: 'Deleted an nodeCommodity'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-node-commodity-delete-popup',
    template: ''
})
export class NodeCommodityDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ nodeCommodity }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(NodeCommodityDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.nodeCommodity = nodeCommodity;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
