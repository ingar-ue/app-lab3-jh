import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INodeCommodity } from 'app/shared/model/node-commodity.model';

@Component({
    selector: 'jhi-node-commodity-detail',
    templateUrl: './node-commodity-detail.component.html'
})
export class NodeCommodityDetailComponent implements OnInit {
    nodeCommodity: INodeCommodity;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ nodeCommodity }) => {
            this.nodeCommodity = nodeCommodity;
        });
    }

    previousState() {
        window.history.back();
    }
}
