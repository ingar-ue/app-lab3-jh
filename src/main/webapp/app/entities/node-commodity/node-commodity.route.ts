import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NodeCommodity } from 'app/shared/model/node-commodity.model';
import { NodeCommodityService } from './node-commodity.service';
import { NodeCommodityComponent } from './node-commodity.component';
import { NodeCommodityDetailComponent } from './node-commodity-detail.component';
import { NodeCommodityUpdateComponent } from './node-commodity-update.component';
import { NodeCommodityDeletePopupComponent } from './node-commodity-delete-dialog.component';
import { INodeCommodity } from 'app/shared/model/node-commodity.model';

@Injectable({ providedIn: 'root' })
export class NodeCommodityResolve implements Resolve<INodeCommodity> {
    constructor(private service: NodeCommodityService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<NodeCommodity> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<NodeCommodity>) => response.ok),
                map((nodeCommodity: HttpResponse<NodeCommodity>) => nodeCommodity.body)
            );
        }
        return of(new NodeCommodity());
    }
}

export const nodeCommodityRoute: Routes = [
    {
        path: 'node-commodity',
        component: NodeCommodityComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.nodeCommodity.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'node-commodity/:id/view',
        component: NodeCommodityDetailComponent,
        resolve: {
            nodeCommodity: NodeCommodityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.nodeCommodity.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'node-commodity/new',
        component: NodeCommodityUpdateComponent,
        resolve: {
            nodeCommodity: NodeCommodityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.nodeCommodity.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'node-commodity/:id/edit',
        component: NodeCommodityUpdateComponent,
        resolve: {
            nodeCommodity: NodeCommodityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.nodeCommodity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const nodeCommodityPopupRoute: Routes = [
    {
        path: 'node-commodity/:id/delete',
        component: NodeCommodityDeletePopupComponent,
        resolve: {
            nodeCommodity: NodeCommodityResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLabApp.nodeCommodity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
