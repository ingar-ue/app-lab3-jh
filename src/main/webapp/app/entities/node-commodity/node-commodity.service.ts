import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { INodeCommodity } from 'app/shared/model/node-commodity.model';

type EntityResponseType = HttpResponse<INodeCommodity>;
type EntityArrayResponseType = HttpResponse<INodeCommodity[]>;

@Injectable({ providedIn: 'root' })
export class NodeCommodityService {
    public resourceUrl = SERVER_API_URL + 'api/node-commodities';

    constructor(private http: HttpClient) {}

    create(nodeCommodity: INodeCommodity): Observable<EntityResponseType> {
        return this.http.post<INodeCommodity>(this.resourceUrl, nodeCommodity, { observe: 'response' });
    }

    update(nodeCommodity: INodeCommodity): Observable<EntityResponseType> {
        return this.http.put<INodeCommodity>(this.resourceUrl, nodeCommodity, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<INodeCommodity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<INodeCommodity[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
