import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { IOptimizationPoint1, IResult } from 'app/shared/model/optimization-point-1.model';
import { Principal } from 'app/core';

import { OptimizationPoint1Service } from './optimization-point-1.service';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';
import { IEnvConfig } from 'app/shared/model/env-config.model';
import { Observable, interval } from 'rxjs';
import { Flow, IArc } from 'app/shared/model/arc.model';
import { INode, Remaining } from 'app/shared/model/node.model';
import { IArcCommodity } from 'app/shared/model/arc-commodity.model';
import { INodeCommodity } from 'app/shared/model/node-commodity.model';
import { Commodity } from 'app/shared/model/commodity.model';

@Component({
    selector: 'jhi-optimization-point-1',
    templateUrl: './optimization-point-1.component.html'
})
export class OptimizationPoint1Component implements OnInit, OnDestroy {
    currentAccount: any;
    eventSubscriber: Subscription;
    optimizationPoint1: IOptimizationPoint1;
    modelRunDef: IModelRunDef;
    result: IResult;
    modelRunDefChange = 0;
    objFunctions: IModelObjFunction[];
    configs: IModelRunConfig[];
    configValuesLoaded = true;
    executingOP2: boolean;
    activeTabId: string;
    alertClosed: boolean;
    alertMessage = '';
    sub;

    /******************* TABLE DATA **************************/
    // 1 - nodes
    columnDefs1 = [{ headerName: 'Nombre', field: 'name', editable: false }];
    // 2 - arcs
    columnDefs2 = [
        { headerName: 'Nodo Inicial', field: 'node1Name', editable: false },
        { headerName: 'Nodo Final', field: 'node2Name', editable: false },
        { headerName: 'Capacidad', field: 'capacity', editable: false },
        { headerName: 'Atacable', field: 'attackable', editable: false }
    ];
    // 3 - commodities
    columnDefs3 = [{ headerName: 'Nombre', field: 'name', editable: false }];
    // 4 - node commodities
    columnDefs4 = [
        { headerName: 'Nodo', field: 'nodeName', editable: false },
        { headerName: 'Mercancía', field: 'commodityName', editable: false },
        { headerName: 'Oferta/Demanda', field: 'supplyDemand', editable: false }
    ];
    // 5 - arc commodities
    columnDefs5 = [
        { headerName: 'Arco', field: 'arcName', editable: false },
        { headerName: 'Mercancía', field: 'commodityName', editable: false },
        { headerName: 'Costo de Transporte', field: 'cost', editable: false },
        { headerName: 'Impacto del transporte en la Capacidad', field: 'capacity', editable: false }
    ];

    rowData1: any;
    rowData2: any;
    rowData3: any;
    rowData4: any;
    rowData5: any;

    grid1IsValid = false;
    grid2IsValid = false;
    grid3IsValid = false;
    grid4IsValid = false;
    grid5IsValid = false;

    grid1ValidMsg: String;
    grid2ValidMsg: String;
    grid3ValidMsg: String;
    grid4ValidMsg: String;
    grid5ValidMsg: String;

    gridApi: any;
    gridColumnApi: any;

    /************************ CLASS **************************/
    constructor(
        private optimizationPoint1Service: OptimizationPoint1Service,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal
    ) {
        this.modelRunDef = {};
        this.executingOP2 = false;
        this.activeTabId = 'dataTab';
        this.alertClosed = true;
    }

    ngOnInit() {
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.loadCase();
        this.registerChangeInOptimizationPoint1s();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInOptimizationPoint1s() {
        this.eventSubscriber = this.eventManager.subscribe('optimizationPoint1ListModification', response => this.reset());
    }

    reset() {
        this.loadCase();
        this.registerChangeInOptimizationPoint1s();
        // this.chartInit();
    }

    loadCase() {
        this.optimizationPoint1Service.loadCase().subscribe((res: HttpResponse<IOptimizationPoint1>) => {
            this.optimizationPoint1 = res.body;
            this.optimizationPoint1Service.checkRunning().subscribe((res1: HttpResponse<IModelRunDef>) => {
                if (res1.body) {
                    this.modelRunDef = res1.body;
                    this.modelRunDefChange++;
                    this.activeTabId = 'processTab';
                } else {
                    this.modelRunDef = this.optimizationPoint1.modelRunDef;
                    this.modelRunDefChange++;
                    this.activeTabId = 'dataTab';
                }
            });

            // Populate tables
            this.rowData1 = this.optimizationPoint1.nodes;
            this.rowData2 = this.optimizationPoint1.arcs;
            this.rowData3 = this.optimizationPoint1.commodities;
            this.rowData4 = this.optimizationPoint1.nodeCommodities;
            this.rowData5 = this.optimizationPoint1.arcCommodities;

            // Validate data grids and set message
            this.validateCaseData();

            this.optimizationPoint1Service
                .loadObjFunctions(this.optimizationPoint1.modelDefId)
                .subscribe((res1: HttpResponse<IModelObjFunction[]>) => {
                    this.objFunctions = res1.body;
                });
            this.optimizationPoint1Service
                .loadConfigs(this.optimizationPoint1.modelDefId)
                .subscribe((res2: HttpResponse<IModelRunConfig[]>) => {
                    this.configs = res2.body;
                });
        });
    }

    run() {
        let url = '';
        this.optimizationPoint1Service.getSolverServerURL().subscribe((res1: HttpResponse<IEnvConfig>) => {
            url = res1.body.value;
            this.modelRunDef.progress = 5;
            this.sub = interval(5000).subscribe(val => {
                if (this.modelRunDef.progress < 95) {
                    this.modelRunDef.progress += 5;
                }
            });
            this.optimizationPoint1.modelRunConfigs = this.configs;
            this.optimizationPoint1Service
                .postCSV(url, this.generateInputCSV(this.optimizationPoint1), this.configs[0].value)
                .subscribe((res: JSON) => {
                    console.log(JSON.stringify(res));
                    this.sub.unsubscribe();
                    this.modelRunDef.progress = 100;
                    this.modelRunDef.status = 'FINISHED';
                    this.modelRunDef.userResult = this.mapResult(res);
                    this.optimizationPoint1Service.saveModelRunDef(this.modelRunDef).subscribe(res2 => {
                        this.fireSuccessAlert('La ejecución ha finalizado, puede visualizar los resultados en la pestaña correspondiente');
                        this.activeTabId = 'resultsTab';
                    });
                });
        });
    }

    fireSuccessAlert(msg: string) {
        this.alertMessage = msg;
        this.alertClosed = false;
        setTimeout(() => (this.alertClosed = true), 10000);
    }

    configChange() {
        this.configValuesLoaded = true;
        for (const config of this.configs) {
            if (!config.value) {
                this.configValuesLoaded = false;
            }
        }
    }

    /******************* TABLE FUNCTIONS **************************/
    validateCaseData() {
        // grid 1
        if (this.rowData1.length > 0) {
            this.grid1IsValid = true;
            this.grid1ValidMsg = 'Datos correctos';
        } else {
            this.grid1IsValid = false;
            this.grid1ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 2
        if (this.rowData2.length > 0) {
            this.grid2IsValid = true;
            this.grid2ValidMsg = 'Datos correctos';
        } else {
            this.grid2IsValid = false;
            this.grid2ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 3
        if (this.rowData3.length > 0) {
            this.grid3IsValid = true;
            this.grid3ValidMsg = 'Datos correctos';
        } else {
            this.grid3IsValid = false;
            this.grid3ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 4
        if (this.rowData4.length > 0) {
            this.grid4IsValid = true;
            this.grid4ValidMsg = 'Datos correctos';
        } else {
            this.grid4IsValid = false;
            this.grid4ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 5
        if (this.rowData5.length > 0) {
            this.grid5IsValid = true;
            this.grid5ValidMsg = 'Datos correctos';
        } else {
            this.grid5IsValid = false;
            this.grid5ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
    }

    onGridReady(params, id) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        // Setea ancho de las columnas para llenar ancho de la tabla
        params.api.sizeColumnsToFit();
        // Si la cantidad de filas a mostrar es mayor que 25 recorta el largo de la tabla
        console.log('params.api.getDisplayedRowCount() ' + params.api.getDisplayedRowCount());
        if (params.api.getDisplayedRowCount() > 10) {
            params.api.setDomLayout('normal');
            const theGrid: HTMLElement = <HTMLElement>document.querySelector('#' + id);
            theGrid.style.height = '300px';
        } else {
            params.api.setDomLayout('autoHeight');
            const theGrid: HTMLElement = <HTMLElement>document.querySelector('#' + id);
            theGrid.style.height = '';
        }
    }

    /******************* CSV INPUT FUNCTIONS **************************/
    convertArcToCSV(arcs: IArc[], columnDelimiter = ',', lineDelimiter = '\n') {
        let result, keys;
        keys = ['StartNode', 'EndNode', 'Capacity', 'Attackable'];
        result = '';
        result += keys.join(columnDelimiter);
        result += lineDelimiter;

        arcs.forEach(function(item) {
            result += item.node1Name + columnDelimiter;
            result += item.node2Name + columnDelimiter;
            result += item.capacity + columnDelimiter;
            result += +item.attackable + lineDelimiter;
        });

        return result;
    }

    convertNodeToCSV(nodes: INode[], columnDelimiter = ',', lineDelimiter = '\n') {
        let result, keys;
        keys = ['Node'];
        result = '';
        result += keys.join(columnDelimiter);
        result += lineDelimiter;

        nodes.forEach(function(item) {
            result += item.name + lineDelimiter;
        });

        return result;
    }

    convertArcCommoditiesToCSV(arcCom: IArcCommodity[], columnDelimiter = ',', lineDelimiter = '\n') {
        let result, keys;
        keys = ['StartNode', 'EndNode', 'Commodity', 'Cost', 'Capacity'];
        result = '';
        result += keys.join(columnDelimiter);
        result += lineDelimiter;

        arcCom.forEach(function(item) {
            result += item.arcNode1Name + columnDelimiter;
            result += item.arcNode2Name + columnDelimiter;
            result += item.commodityName + columnDelimiter;
            result += item.cost + columnDelimiter;
            result += +item.capacity + lineDelimiter;
        });

        return result;
    }

    convertNodeCommoditiesToCSV(nodeCom: INodeCommodity[], columnDelimiter = ',', lineDelimiter = '\n') {
        let result, keys;
        keys = ['Node', 'Commodity', 'SupplyDemand'];
        result = '';
        result += keys.join(columnDelimiter);
        result += lineDelimiter;

        nodeCom.forEach(function(item) {
            result += item.nodeName + columnDelimiter;
            result += item.commodityName + columnDelimiter;
            result += +item.supplyDemand + lineDelimiter;
        });

        return result;
    }

    generateInputCSV(opt1: IOptimizationPoint1): File[] {
        let file, filename, csv;
        const list: File[] = [];

        csv = this.convertArcToCSV(opt1.arcs);
        filename = 'arcs.csv';
        file = new File([csv], filename, { type: 'text/csv' });
        list.push(file);

        csv = this.convertNodeToCSV(opt1.nodes);
        filename = 'nodes.csv';
        file = new File([csv], filename, { type: 'text/csv' });
        list.push(file);

        csv = this.convertNodeCommoditiesToCSV(opt1.nodeCommodities);
        filename = 'nodeCommodities.csv';
        file = new File([csv], filename, { type: 'text/csv' });
        list.push(file);

        csv = this.convertArcCommoditiesToCSV(opt1.arcCommodities);
        filename = 'arcCommodities.csv';
        file = new File([csv], filename, { type: 'text/csv' });
        list.push(file);

        return list;
    }

    /******************* JSON OUTPUT FUNCTIONS **************************/

    mapResult(res: JSON): IResult {
        const result: IResult = {};

        result.arcs = this.optimizationPoint1.arcs.concat();
        result.nodes = this.optimizationPoint1.nodes.concat();
        result.total = res['total'];

        const interdicts = res['interdicts'];
        const remainingSupplies = res['remaining_supply'];
        const remainingDemands = res['remaining_demand'];
        const flows = res['flow'];

        for (const arc of result.arcs) {
            arc.interdicted = false;
            for (let i = 0; i < interdicts.length; i++) {
                const int = interdicts[i];
                if (arc.node1Name === int['node1'] && arc.node2Name === int['node2']) {
                    arc.interdicted = true;
                    break;
                }
            }
            arc.flow = [];
            for (let i = 0; i < flows.length; i++) {
                const f = flows[i];
                if (arc.node1Name === f['node1'] && arc.node2Name === f['node2']) {
                    const resultFlow = new Flow();
                    resultFlow.setCommodity(f['commodity'], this.optimizationPoint1.commodities);
                    resultFlow.quantity = f['quantity'];
                    arc.flow.push(resultFlow);
                }
            }
        }

        for (const node of result.nodes) {
            for (let i = 0; i < remainingSupplies.length; i++) {
                const rs = remainingSupplies[i];
                if (node.name === rs['node']) {
                    const resultRS = new Remaining();
                    resultRS.setCommodity(rs['commodity'], this.optimizationPoint1.commodities);
                    resultRS.remaining = rs['supply'];
                    node.remainingSupply = resultRS;
                }
            }
            for (let i = 0; i < remainingDemands.length; i++) {
                const rd = remainingDemands[i];
                if (node.name === rd['node']) {
                    const resultRD = new Remaining();
                    resultRD.setCommodity(rd['commodity'], this.optimizationPoint1.commodities);
                    resultRD.remaining = rd['supply'];
                    node.remainingDemand = resultRD;
                }
            }
        }

        return result;
    }
}
