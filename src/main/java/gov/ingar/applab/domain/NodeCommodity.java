package gov.ingar.applab.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A NodeCommodity.
 */
@Entity
@Table(name = "node_commodity")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NodeCommodity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "supply_demand", nullable = false)
    private Integer supplyDemand;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Commodity commodity;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Node node;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSupplyDemand() {
        return supplyDemand;
    }

    public NodeCommodity supplyDemand(Integer supplyDemand) {
        this.supplyDemand = supplyDemand;
        return this;
    }

    public void setSupplyDemand(Integer supplyDemand) {
        this.supplyDemand = supplyDemand;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public NodeCommodity commodity(Commodity commodity) {
        this.commodity = commodity;
        return this;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public Node getNode() {
        return node;
    }

    public NodeCommodity node(Node node) {
        this.node = node;
        return this;
    }

    public void setNode(Node node) {
        this.node = node;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NodeCommodity nodeCommodity = (NodeCommodity) o;
        if (nodeCommodity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), nodeCommodity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NodeCommodity{" +
            "id=" + getId() +
            ", supplyDemand=" + getSupplyDemand() +
            "}";
    }
}
