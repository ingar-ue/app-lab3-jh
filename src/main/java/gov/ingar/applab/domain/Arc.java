package gov.ingar.applab.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Arc.
 */
@Entity
@Table(name = "arc")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Arc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "capacity", nullable = false)
    private Integer capacity;

    @NotNull
    @Column(name = "attackable", nullable = false)
    private Boolean attackable;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Node node1;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Node node2;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public Arc capacity(Integer capacity) {
        this.capacity = capacity;
        return this;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Boolean isAttackable() {
        return attackable;
    }

    public Arc attackable(Boolean attackable) {
        this.attackable = attackable;
        return this;
    }

    public void setAttackable(Boolean attackable) {
        this.attackable = attackable;
    }

    public Node getNode1() {
        return node1;
    }

    public Arc node1(Node node) {
        this.node1 = node;
        return this;
    }

    public void setNode1(Node node) {
        this.node1 = node;
    }

    public Node getNode2() {
        return node2;
    }

    public Arc node2(Node node) {
        this.node2 = node;
        return this;
    }

    public void setNode2(Node node) {
        this.node2 = node;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Arc arc = (Arc) o;
        if (arc.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), arc.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Arc{" +
            "id=" + getId() +
            ", capacity=" + getCapacity() +
            ", attackable='" + isAttackable() + "'" +
            "}";
    }
}
