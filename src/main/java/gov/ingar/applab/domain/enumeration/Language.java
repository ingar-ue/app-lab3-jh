package gov.ingar.applab.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    GAMS, PYOMO, ORTOOLS
}
