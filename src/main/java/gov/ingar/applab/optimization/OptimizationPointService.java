package gov.ingar.applab.optimization;

import gov.ingar.applab.domain.ModelDef;
import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.optimization.dto.OptimizationPointDTO;
import gov.ingar.applab.repository.*;
import gov.ingar.applab.security.SecurityUtils;
import gov.ingar.applab.service.ModelConfigService;
import gov.ingar.applab.service.ModelDefService;
import gov.ingar.applab.service.dto.*;
import gov.ingar.applab.service.mapper.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class
OptimizationPointService {

    private final Logger log = LoggerFactory.getLogger(OptimizationPointService.class);

    private final ModelDefService modelDefService;
    private final ModelConfigService modelConfigService;

    private final NodeRepository nodeRepository;
    private final ArcRepository arcRepository;
    private final CommodityRepository commodityRepository;
    private final ArcCommodityRepository arcCommodityRepository;
    private final NodeCommodityRepository nodeCommodityRepository;

    private final NodeMapper nodeMapper;
    private final ArcMapper arcMapper;
    private final CommodityMapper commodityMapper;
    private final ArcCommodityMapper arcCommodityMapper;
    private final NodeCommodityMapper nodeCommodityMapper;


    public OptimizationPointService(NodeRepository nodeRepository, ArcRepository arcRepository, CommodityRepository commodityRepository,
                                    ArcCommodityRepository arcCommodityRepository, NodeCommodityRepository nodeCommodityRepository,
                                    NodeMapper nodeMapper, ArcMapper arcMapper, CommodityMapper commodityMapper, ArcCommodityMapper arcCommodityMapper,
                                    NodeCommodityMapper nodeCommodityMapper, ModelDefService modelDefService, ModelConfigService modelConfigService) {
        this.modelDefService = modelDefService;
        this.modelConfigService = modelConfigService;

        this.nodeRepository = nodeRepository;
        this.arcRepository = arcRepository;
        this.commodityRepository = commodityRepository;
        this.nodeCommodityRepository = nodeCommodityRepository;
        this.arcCommodityRepository = arcCommodityRepository;

        this.nodeMapper = nodeMapper;
        this.arcMapper = arcMapper;
        this.commodityMapper = commodityMapper;
        this.nodeCommodityMapper = nodeCommodityMapper;
        this.arcCommodityMapper = arcCommodityMapper;

    }

    public OptimizationPointDTO loadCase(){
        ModelRunDef runDef = new ModelRunDef();
        ModelDef def = modelDefService.findOneByName("Multi Commodity Flow Interdiction");
        OptimizationPointDTO op1 = loadCaseInput();

        runDef.setName(def.getName());
        runDef.setLanguage(def.getLanguage());
        runDef.setSolver(def.getSolver());
        runDef.setVersion(def.getCurrentVersion());
        runDef.setProgress(0);
        runDef.setUser(SecurityUtils.getCurrentUsername());

        op1.setModelRunDef(runDef);
        op1.setModelDefId(def.getId());

        return op1;
    }

    private OptimizationPointDTO loadCaseInput(){
        OptimizationPointDTO dto = new OptimizationPointDTO();

        dto.setNodes(this.nodeList());
        dto.setArcs(this.arcList());
        dto.setCommodities(this.commodityList());
        dto.setArcCommodities(this.arcCommodityList());
        dto.setNodeCommodities(this.nodeCommodityList());

        return dto;
    }

    private List<NodeDTO> nodeList() {
        return nodeMapper.toDto(nodeRepository.findAll());
    }

    private List<ArcDTO> arcList() {
        return arcMapper.toDto(arcRepository.findAll());
    }

    private List<CommodityDTO> commodityList() {
        return commodityMapper.toDto(commodityRepository.findAll());
    }

    private List<NodeCommodityDTO> nodeCommodityList() {
        return nodeCommodityMapper.toDto(nodeCommodityRepository.findAll());
    }

    private List<ArcCommodityDTO> arcCommodityList() {
        return arcCommodityMapper.toDto(arcCommodityRepository.findAll());
    }

//    @Override
//    public void runOptimizationPoint(OptimizationPointDTO optimizationPointDTO) {
//        gamsOptimizationPoint1Runner.runOptimizationPoint1((OptimizationPoint1DTO) optimizationPointDTO);
//    }

}
