package gov.ingar.applab.optimization.dto;

import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.service.dto.*;

import java.io.Serializable;
import java.util.List;

public class OptimizationPointDTO implements Serializable {
    List<NodeDTO> nodes;
    List<ArcDTO> arcs;
    List<CommodityDTO> commodities;
    List<ArcCommodityDTO> arcCommodities;
    List<NodeCommodityDTO> nodeCommodities;
    private ModelRunDef modelRunDef;
    private Long modelDefId;

    public List<NodeDTO> getNodes() {
        return nodes;
    }

    public void setNodes(List<NodeDTO> nodes) {
        this.nodes = nodes;
    }

    public List<ArcDTO> getArcs() {
        return arcs;
    }

    public void setArcs(List<ArcDTO> arcs) {
        this.arcs = arcs;
    }

    public List<CommodityDTO> getCommodities() {
        return commodities;
    }

    public void setCommodities(List<CommodityDTO> commodities) {
        this.commodities = commodities;
    }

    public List<ArcCommodityDTO> getArcCommodities() {
        return arcCommodities;
    }

    public void setArcCommodities(List<ArcCommodityDTO> arcCommodities) {
        this.arcCommodities = arcCommodities;
    }

    public List<NodeCommodityDTO> getNodeCommodities() {
        return nodeCommodities;
    }

    public void setNodeCommodities(List<NodeCommodityDTO> nodeCommodities) {
        this.nodeCommodities = nodeCommodities;
    }

    public ModelRunDef getModelRunDef() {
        return modelRunDef;
    }

    public void setModelRunDef(ModelRunDef modelRunDef) {
        this.modelRunDef = modelRunDef;
    }

    public Long getModelDefId() {
        return modelDefId;
    }

    public void setModelDefId(Long modelDefId) {
        this.modelDefId = modelDefId;
    }
}
