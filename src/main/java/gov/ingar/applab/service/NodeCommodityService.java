package gov.ingar.applab.service;

import gov.ingar.applab.domain.NodeCommodity;
import gov.ingar.applab.repository.NodeCommodityRepository;
import gov.ingar.applab.service.dto.NodeCommodityDTO;
import gov.ingar.applab.service.mapper.NodeCommodityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing NodeCommodity.
 */
@Service
@Transactional
public class NodeCommodityService {

    private final Logger log = LoggerFactory.getLogger(NodeCommodityService.class);

    private final NodeCommodityRepository nodeCommodityRepository;

    private final NodeCommodityMapper nodeCommodityMapper;

    public NodeCommodityService(NodeCommodityRepository nodeCommodityRepository, NodeCommodityMapper nodeCommodityMapper) {
        this.nodeCommodityRepository = nodeCommodityRepository;
        this.nodeCommodityMapper = nodeCommodityMapper;
    }

    /**
     * Save a nodeCommodity.
     *
     * @param nodeCommodityDTO the entity to save
     * @return the persisted entity
     */
    public NodeCommodityDTO save(NodeCommodityDTO nodeCommodityDTO) {
        log.debug("Request to save NodeCommodity : {}", nodeCommodityDTO);

        NodeCommodity nodeCommodity = nodeCommodityMapper.toEntity(nodeCommodityDTO);
        nodeCommodity = nodeCommodityRepository.save(nodeCommodity);
        return nodeCommodityMapper.toDto(nodeCommodity);
    }

    /**
     * Get all the nodeCommodities.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NodeCommodityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NodeCommodities");
        return nodeCommodityRepository.findAll(pageable)
            .map(nodeCommodityMapper::toDto);
    }


    /**
     * Get one nodeCommodity by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<NodeCommodityDTO> findOne(Long id) {
        log.debug("Request to get NodeCommodity : {}", id);
        return nodeCommodityRepository.findById(id)
            .map(nodeCommodityMapper::toDto);
    }

    /**
     * Delete the nodeCommodity by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NodeCommodity : {}", id);
        nodeCommodityRepository.deleteById(id);
    }
}
