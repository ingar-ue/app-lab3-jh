package gov.ingar.applab.service;

import gov.ingar.applab.domain.ArcCommodity;
import gov.ingar.applab.repository.ArcCommodityRepository;
import gov.ingar.applab.service.dto.ArcCommodityDTO;
import gov.ingar.applab.service.mapper.ArcCommodityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing ArcCommodity.
 */
@Service
@Transactional
public class ArcCommodityService {

    private final Logger log = LoggerFactory.getLogger(ArcCommodityService.class);

    private final ArcCommodityRepository arcCommodityRepository;

    private final ArcCommodityMapper arcCommodityMapper;

    public ArcCommodityService(ArcCommodityRepository arcCommodityRepository, ArcCommodityMapper arcCommodityMapper) {
        this.arcCommodityRepository = arcCommodityRepository;
        this.arcCommodityMapper = arcCommodityMapper;
    }

    /**
     * Save a arcCommodity.
     *
     * @param arcCommodityDTO the entity to save
     * @return the persisted entity
     */
    public ArcCommodityDTO save(ArcCommodityDTO arcCommodityDTO) {
        log.debug("Request to save ArcCommodity : {}", arcCommodityDTO);

        ArcCommodity arcCommodity = arcCommodityMapper.toEntity(arcCommodityDTO);
        arcCommodity = arcCommodityRepository.save(arcCommodity);
        return arcCommodityMapper.toDto(arcCommodity);
    }

    /**
     * Get all the arcCommodities.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ArcCommodityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ArcCommodities");
        return arcCommodityRepository.findAll(pageable)
            .map(arcCommodityMapper::toDto);
    }


    /**
     * Get one arcCommodity by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ArcCommodityDTO> findOne(Long id) {
        log.debug("Request to get ArcCommodity : {}", id);
        return arcCommodityRepository.findById(id)
            .map(arcCommodityMapper::toDto);
    }

    /**
     * Delete the arcCommodity by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ArcCommodity : {}", id);
        arcCommodityRepository.deleteById(id);
    }
}
