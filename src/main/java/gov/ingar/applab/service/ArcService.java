package gov.ingar.applab.service;

import gov.ingar.applab.domain.Arc;
import gov.ingar.applab.repository.ArcRepository;
import gov.ingar.applab.service.dto.ArcDTO;
import gov.ingar.applab.service.mapper.ArcMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Arc.
 */
@Service
@Transactional
public class ArcService {

    private final Logger log = LoggerFactory.getLogger(ArcService.class);

    private final ArcRepository arcRepository;

    private final ArcMapper arcMapper;

    public ArcService(ArcRepository arcRepository, ArcMapper arcMapper) {
        this.arcRepository = arcRepository;
        this.arcMapper = arcMapper;
    }

    /**
     * Save a arc.
     *
     * @param arcDTO the entity to save
     * @return the persisted entity
     */
    public ArcDTO save(ArcDTO arcDTO) {
        log.debug("Request to save Arc : {}", arcDTO);

        Arc arc = arcMapper.toEntity(arcDTO);
        arc = arcRepository.save(arc);
        return arcMapper.toDto(arc);
    }

    /**
     * Get all the arcs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ArcDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Arcs");
        return arcRepository.findAll(pageable)
            .map(arcMapper::toDto);
    }


    /**
     * Get one arc by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ArcDTO> findOne(Long id) {
        log.debug("Request to get Arc : {}", id);
        return arcRepository.findById(id)
            .map(arcMapper::toDto);
    }

    /**
     * Delete the arc by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Arc : {}", id);
        arcRepository.deleteById(id);
    }
}
