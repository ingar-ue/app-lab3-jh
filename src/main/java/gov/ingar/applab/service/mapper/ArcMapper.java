package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ArcDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Arc and its DTO ArcDTO.
 */
@Mapper(componentModel = "spring", uses = {NodeMapper.class})
public interface ArcMapper extends EntityMapper<ArcDTO, Arc> {

    @Mapping(source = "node1.id", target = "node1Id")
    @Mapping(source = "node2.id", target = "node2Id")
    @Mapping(source = "node1.name", target = "node1Name")
    @Mapping(source = "node2.name", target = "node2Name")
    ArcDTO toDto(Arc arc);

    @Mapping(source = "node1Id", target = "node1")
    @Mapping(source = "node2Id", target = "node2")
    Arc toEntity(ArcDTO arcDTO);

    default Arc fromId(Long id) {
        if (id == null) {
            return null;
        }
        Arc arc = new Arc();
        arc.setId(id);
        return arc;
    }
}
