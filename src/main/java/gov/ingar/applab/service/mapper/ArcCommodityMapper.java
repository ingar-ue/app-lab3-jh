package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ArcCommodityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ArcCommodity and its DTO ArcCommodityDTO.
 */
@Mapper(componentModel = "spring", uses = {CommodityMapper.class, ArcMapper.class})
public interface ArcCommodityMapper extends EntityMapper<ArcCommodityDTO, ArcCommodity> {

    @Mapping(source = "commodity.id", target = "commodityId")
    @Mapping(source = "arc.id", target = "arcId")
    @Mapping(source = "commodity.name", target = "commodityName")
    @Mapping(source = "arc.node1.name", target = "arcNode1Name")
    @Mapping(source = "arc.node2.name", target = "arcNode2Name")
    ArcCommodityDTO toDto(ArcCommodity arcCommodity);

    @Mapping(source = "commodityId", target = "commodity")
    @Mapping(source = "arcId", target = "arc")
    ArcCommodity toEntity(ArcCommodityDTO arcCommodityDTO);

    default ArcCommodity fromId(Long id) {
        if (id == null) {
            return null;
        }
        ArcCommodity arcCommodity = new ArcCommodity();
        arcCommodity.setId(id);
        return arcCommodity;
    }
}
