package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ModelObjFunctionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ModelObjFunction and its DTO ModelObjFunctionDTO.
 */
@Mapper(componentModel = "spring", uses = {ModelDefMapper.class})
public interface ModelObjFunctionMapper extends EntityMapper<ModelObjFunctionDTO, ModelObjFunction> {

    @Mapping(source = "modelDef.id", target = "modelDefId")
    ModelObjFunctionDTO toDto(ModelObjFunction modelObjFunction);

    @Mapping(source = "modelDefId", target = "modelDef")
    ModelObjFunction toEntity(ModelObjFunctionDTO modelObjFunctionDTO);

    default ModelObjFunction fromId(Long id) {
        if (id == null) {
            return null;
        }
        ModelObjFunction modelObjFunction = new ModelObjFunction();
        modelObjFunction.setId(id);
        return modelObjFunction;
    }
}
