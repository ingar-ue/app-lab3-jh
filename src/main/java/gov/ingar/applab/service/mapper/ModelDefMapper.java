package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ModelDefDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ModelDef and its DTO ModelDefDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ModelDefMapper extends EntityMapper<ModelDefDTO, ModelDef> {



    default ModelDef fromId(Long id) {
        if (id == null) {
            return null;
        }
        ModelDef modelDef = new ModelDef();
        modelDef.setId(id);
        return modelDef;
    }
}
