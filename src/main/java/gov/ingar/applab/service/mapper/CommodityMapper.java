package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.CommodityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Commodity and its DTO CommodityDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CommodityMapper extends EntityMapper<CommodityDTO, Commodity> {



    default Commodity fromId(Long id) {
        if (id == null) {
            return null;
        }
        Commodity commodity = new Commodity();
        commodity.setId(id);
        return commodity;
    }
}
