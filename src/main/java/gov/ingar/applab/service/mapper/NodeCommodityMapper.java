package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.NodeCommodityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity NodeCommodity and its DTO NodeCommodityDTO.
 */
@Mapper(componentModel = "spring", uses = {CommodityMapper.class, NodeMapper.class})
public interface NodeCommodityMapper extends EntityMapper<NodeCommodityDTO, NodeCommodity> {

    @Mapping(source = "commodity.id", target = "commodityId")
    @Mapping(source = "node.id", target = "nodeId")
    @Mapping(source = "commodity.name", target = "commodityName")
    @Mapping(source = "node.name", target = "nodeName")
    NodeCommodityDTO toDto(NodeCommodity nodeCommodity);

    @Mapping(source = "commodityId", target = "commodity")
    @Mapping(source = "nodeId", target = "node")
    NodeCommodity toEntity(NodeCommodityDTO nodeCommodityDTO);

    default NodeCommodity fromId(Long id) {
        if (id == null) {
            return null;
        }
        NodeCommodity nodeCommodity = new NodeCommodity();
        nodeCommodity.setId(id);
        return nodeCommodity;
    }
}
