package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ModelConfig entity.
 */
public class ModelConfigDTO implements Serializable {

    private Long id;

    @NotNull
    private String param;

    private String value;

    private String description;

    private Long modelDefId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getModelDefId() {
        return modelDefId;
    }

    public void setModelDefId(Long modelDefId) {
        this.modelDefId = modelDefId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModelConfigDTO modelConfigDTO = (ModelConfigDTO) o;
        if (modelConfigDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelConfigDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelConfigDTO{" +
            "id=" + getId() +
            ", param='" + getParam() + "'" +
            ", value='" + getValue() + "'" +
            ", description='" + getDescription() + "'" +
            ", modelDef=" + getModelDefId() +
            "}";
    }
}
