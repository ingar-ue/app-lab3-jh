package gov.ingar.applab.service.dto;

import java.io.*;
import java.util.List;

public class ResultDTO implements Serializable {

    private Double total;
    private List<ArcDTO> arcs;
    private List<NodeDTO> nodes;

    public ResultDTO() {
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<ArcDTO> getArcs() {
        return arcs;
    }

    public void setArcs(List<ArcDTO> arcs) {
        this.arcs = arcs;
    }

    public List<NodeDTO> getNodes() {
        return nodes;
    }

    public void setNodes(List<NodeDTO> nodes) {
        this.nodes = nodes;
    }

    public byte[] toStream() {
        // Reference for stream of bytes
        byte[] stream = null;
        // ObjectOutputStream is used to convert a Java object into OutputStream
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos);) {
            oos.writeObject(this);
            stream = baos.toByteArray();
        } catch (IOException e) {
            // Error in serialization
            e.printStackTrace();
        }
        return stream;
    }

    public static ResultDTO toResult(byte[] stream) {
        ResultDTO resultDTO = null;

        try (ByteArrayInputStream bais = new ByteArrayInputStream(stream);
             ObjectInputStream ois = new ObjectInputStream(bais);) {
            resultDTO = (ResultDTO) ois.readObject();
        } catch (IOException e) {
            // Error in de-serialization
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // You are converting an invalid stream to Student
            e.printStackTrace();
        }
        return resultDTO;
    }
}
