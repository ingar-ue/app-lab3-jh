package gov.ingar.applab.service.dto;

import java.io.Serializable;

public class RemainingDTO implements Serializable {

    private CommodityDTO commodity;
    private Double remaining;

    public RemainingDTO(){}

    public CommodityDTO getCommodity() {
        return commodity;
    }

    public void setCommodity(CommodityDTO commodity) {
        this.commodity = commodity;
    }

    public Double getRemaining() {
        return remaining;
    }

    public void setRemaining(Double remaining) {
        this.remaining = remaining;
    }
}
