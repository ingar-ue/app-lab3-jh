package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the Arc entity.
 */
public class ArcDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer capacity;

    @NotNull
    private Boolean attackable;

    private Long node1Id;

    private Long node2Id;

    private String node1Name;

    private String node2Name;

    //Result variables
    private boolean interdicted;

    private List<FlowDTO> flow;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Boolean isAttackable() {
        return attackable;
    }

    public void setAttackable(Boolean attackable) {
        this.attackable = attackable;
    }

    public Long getNode1Id() {
        return node1Id;
    }

    public void setNode1Id(Long nodeId) {
        this.node1Id = nodeId;
    }

    public Long getNode2Id() {
        return node2Id;
    }

    public void setNode2Id(Long nodeId) {
        this.node2Id = nodeId;
    }

    public Boolean getAttackable() {
        return attackable;
    }

    public String getNode1Name() {
        return node1Name;
    }

    public void setNode1Name(String node1Name) {
        this.node1Name = node1Name;
    }

    public String getNode2Name() {
        return node2Name;
    }

    public void setNode2Name(String node2Name) {
        this.node2Name = node2Name;
    }

    public List<FlowDTO> getFlow() {
        return flow;
    }

    public void setFlow(List<FlowDTO> flow) {
        this.flow = flow;
    }

    public boolean isInterdicted() {
        return interdicted;
    }

    public void setInterdicted(boolean interdicted) {
        this.interdicted = interdicted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ArcDTO arcDTO = (ArcDTO) o;
        if (arcDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), arcDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ArcDTO{" +
            "id=" + getId() +
            ", capacity=" + getCapacity() +
            ", attackable='" + isAttackable() + "'" +
            ", node1=" + getNode1Id() +
            ", node2=" + getNode2Id() +
            "}";
    }
}
