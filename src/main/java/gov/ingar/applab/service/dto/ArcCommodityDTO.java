package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ArcCommodity entity.
 */
public class ArcCommodityDTO implements Serializable {

    private Long id;

    @NotNull
    private Double cost;

    @NotNull
    private Integer capacity;

    private Long commodityId;

    private Long arcId;

    private String commodityName;

    private String arcNode1Name;

    private String arcNode2Name;

    private String arcName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Long getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(Long commodityId) {
        this.commodityId = commodityId;
    }

    public Long getArcId() {
        return arcId;
    }

    public void setArcId(Long arcId) {
        this.arcId = arcId;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getArcName() {
        return this.arcNode1Name + " - " + this.arcNode2Name;
    }

    public String getArcNode1Name() {
        return arcNode1Name;
    }

    public void setArcNode1Name(String arcNode1Name) {
        this.arcNode1Name = arcNode1Name;
    }

    public String getArcNode2Name() {
        return arcNode2Name;
    }

    public void setArcNode2Name(String arcNode2Name) {
        this.arcNode2Name = arcNode2Name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ArcCommodityDTO arcCommodityDTO = (ArcCommodityDTO) o;
        if (arcCommodityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), arcCommodityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ArcCommodityDTO{" +
            "id=" + getId() +
            ", cost=" + getCost() +
            ", capacity=" + getCapacity() +
            ", commodity=" + getCommodityId() +
            ", arc=" + getArcId() +
            "}";
    }
}
