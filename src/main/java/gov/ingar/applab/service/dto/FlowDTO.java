package gov.ingar.applab.service.dto;

import java.io.Serializable;

public class FlowDTO implements Serializable {

    private CommodityDTO commodity;
    private Double quantity;

    public FlowDTO(){}

    public CommodityDTO getCommodity() {
        return commodity;
    }

    public void setCommodity(CommodityDTO commodity) {
        this.commodity = commodity;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
