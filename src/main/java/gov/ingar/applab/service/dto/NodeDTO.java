package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Node entity.
 */
public class NodeDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    //result variables

    private RemainingDTO remainingDemand;

    private RemainingDTO remainingSupply;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RemainingDTO getRemainingDemand() {
        return remainingDemand;
    }

    public void setRemainingDemand(RemainingDTO remainingDemand) {
        this.remainingDemand = remainingDemand;
    }

    public RemainingDTO getRemainingSupply() {
        return remainingSupply;
    }

    public void setRemainingSupply(RemainingDTO remainingSupply) {
        this.remainingSupply = remainingSupply;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NodeDTO nodeDTO = (NodeDTO) o;
        if (nodeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), nodeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NodeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
