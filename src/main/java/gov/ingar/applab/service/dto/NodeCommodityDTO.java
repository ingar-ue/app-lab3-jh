package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the NodeCommodity entity.
 */
public class NodeCommodityDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer supplyDemand;

    private Long commodityId;

    private Long nodeId;

    private String commodityName;

    private String nodeName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSupplyDemand() {
        return supplyDemand;
    }

    public void setSupplyDemand(Integer supplyDemand) {
        this.supplyDemand = supplyDemand;
    }

    public Long getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(Long commodityId) {
        this.commodityId = commodityId;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NodeCommodityDTO nodeCommodityDTO = (NodeCommodityDTO) o;
        if (nodeCommodityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), nodeCommodityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NodeCommodityDTO{" +
            "id=" + getId() +
            ", supplyDemand=" + getSupplyDemand() +
            ", commodity=" + getCommodityId() +
            ", node=" + getNodeId() +
            "}";
    }
}
