package gov.ingar.applab.service;

import gov.ingar.applab.domain.Commodity;
import gov.ingar.applab.repository.CommodityRepository;
import gov.ingar.applab.service.dto.CommodityDTO;
import gov.ingar.applab.service.mapper.CommodityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Commodity.
 */
@Service
@Transactional
public class CommodityService {

    private final Logger log = LoggerFactory.getLogger(CommodityService.class);

    private final CommodityRepository commodityRepository;

    private final CommodityMapper commodityMapper;

    public CommodityService(CommodityRepository commodityRepository, CommodityMapper commodityMapper) {
        this.commodityRepository = commodityRepository;
        this.commodityMapper = commodityMapper;
    }

    /**
     * Save a commodity.
     *
     * @param commodityDTO the entity to save
     * @return the persisted entity
     */
    public CommodityDTO save(CommodityDTO commodityDTO) {
        log.debug("Request to save Commodity : {}", commodityDTO);

        Commodity commodity = commodityMapper.toEntity(commodityDTO);
        commodity = commodityRepository.save(commodity);
        return commodityMapper.toDto(commodity);
    }

    /**
     * Get all the commodities.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CommodityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Commodities");
        return commodityRepository.findAll(pageable)
            .map(commodityMapper::toDto);
    }


    /**
     * Get one commodity by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<CommodityDTO> findOne(Long id) {
        log.debug("Request to get Commodity : {}", id);
        return commodityRepository.findById(id)
            .map(commodityMapper::toDto);
    }

    /**
     * Delete the commodity by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Commodity : {}", id);
        commodityRepository.deleteById(id);
    }
}
