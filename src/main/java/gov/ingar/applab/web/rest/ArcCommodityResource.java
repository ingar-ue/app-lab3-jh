package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.ArcCommodityService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ArcCommodityDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ArcCommodity.
 */
@RestController
@RequestMapping("/api")
public class ArcCommodityResource {

    private final Logger log = LoggerFactory.getLogger(ArcCommodityResource.class);

    private static final String ENTITY_NAME = "arcCommodity";

    private final ArcCommodityService arcCommodityService;

    public ArcCommodityResource(ArcCommodityService arcCommodityService) {
        this.arcCommodityService = arcCommodityService;
    }

    /**
     * POST  /arc-commodities : Create a new arcCommodity.
     *
     * @param arcCommodityDTO the arcCommodityDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new arcCommodityDTO, or with status 400 (Bad Request) if the arcCommodity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/arc-commodities")
    @Timed
    public ResponseEntity<ArcCommodityDTO> createArcCommodity(@Valid @RequestBody ArcCommodityDTO arcCommodityDTO) throws URISyntaxException {
        log.debug("REST request to save ArcCommodity : {}", arcCommodityDTO);
        if (arcCommodityDTO.getId() != null) {
            throw new BadRequestAlertException("A new arcCommodity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ArcCommodityDTO result = arcCommodityService.save(arcCommodityDTO);
        return ResponseEntity.created(new URI("/api/arc-commodities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /arc-commodities : Updates an existing arcCommodity.
     *
     * @param arcCommodityDTO the arcCommodityDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated arcCommodityDTO,
     * or with status 400 (Bad Request) if the arcCommodityDTO is not valid,
     * or with status 500 (Internal Server Error) if the arcCommodityDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/arc-commodities")
    @Timed
    public ResponseEntity<ArcCommodityDTO> updateArcCommodity(@Valid @RequestBody ArcCommodityDTO arcCommodityDTO) throws URISyntaxException {
        log.debug("REST request to update ArcCommodity : {}", arcCommodityDTO);
        if (arcCommodityDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ArcCommodityDTO result = arcCommodityService.save(arcCommodityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, arcCommodityDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /arc-commodities : get all the arcCommodities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of arcCommodities in body
     */
    @GetMapping("/arc-commodities")
    @Timed
    public ResponseEntity<List<ArcCommodityDTO>> getAllArcCommodities(Pageable pageable) {
        log.debug("REST request to get a page of ArcCommodities");
        Page<ArcCommodityDTO> page = arcCommodityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/arc-commodities");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /arc-commodities/:id : get the "id" arcCommodity.
     *
     * @param id the id of the arcCommodityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the arcCommodityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/arc-commodities/{id}")
    @Timed
    public ResponseEntity<ArcCommodityDTO> getArcCommodity(@PathVariable Long id) {
        log.debug("REST request to get ArcCommodity : {}", id);
        Optional<ArcCommodityDTO> arcCommodityDTO = arcCommodityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(arcCommodityDTO);
    }

    /**
     * DELETE  /arc-commodities/:id : delete the "id" arcCommodity.
     *
     * @param id the id of the arcCommodityDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/arc-commodities/{id}")
    @Timed
    public ResponseEntity<Void> deleteArcCommodity(@PathVariable Long id) {
        log.debug("REST request to delete ArcCommodity : {}", id);
        arcCommodityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
