/**
 * View Models used by Spring MVC REST controllers.
 */
package gov.ingar.applab.web.rest.vm;
