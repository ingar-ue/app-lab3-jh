package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.NodeCommodityService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.NodeCommodityDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing NodeCommodity.
 */
@RestController
@RequestMapping("/api")
public class NodeCommodityResource {

    private final Logger log = LoggerFactory.getLogger(NodeCommodityResource.class);

    private static final String ENTITY_NAME = "nodeCommodity";

    private final NodeCommodityService nodeCommodityService;

    public NodeCommodityResource(NodeCommodityService nodeCommodityService) {
        this.nodeCommodityService = nodeCommodityService;
    }

    /**
     * POST  /node-commodities : Create a new nodeCommodity.
     *
     * @param nodeCommodityDTO the nodeCommodityDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new nodeCommodityDTO, or with status 400 (Bad Request) if the nodeCommodity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/node-commodities")
    @Timed
    public ResponseEntity<NodeCommodityDTO> createNodeCommodity(@Valid @RequestBody NodeCommodityDTO nodeCommodityDTO) throws URISyntaxException {
        log.debug("REST request to save NodeCommodity : {}", nodeCommodityDTO);
        if (nodeCommodityDTO.getId() != null) {
            throw new BadRequestAlertException("A new nodeCommodity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NodeCommodityDTO result = nodeCommodityService.save(nodeCommodityDTO);
        return ResponseEntity.created(new URI("/api/node-commodities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /node-commodities : Updates an existing nodeCommodity.
     *
     * @param nodeCommodityDTO the nodeCommodityDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated nodeCommodityDTO,
     * or with status 400 (Bad Request) if the nodeCommodityDTO is not valid,
     * or with status 500 (Internal Server Error) if the nodeCommodityDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/node-commodities")
    @Timed
    public ResponseEntity<NodeCommodityDTO> updateNodeCommodity(@Valid @RequestBody NodeCommodityDTO nodeCommodityDTO) throws URISyntaxException {
        log.debug("REST request to update NodeCommodity : {}", nodeCommodityDTO);
        if (nodeCommodityDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NodeCommodityDTO result = nodeCommodityService.save(nodeCommodityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nodeCommodityDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /node-commodities : get all the nodeCommodities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of nodeCommodities in body
     */
    @GetMapping("/node-commodities")
    @Timed
    public ResponseEntity<List<NodeCommodityDTO>> getAllNodeCommodities(Pageable pageable) {
        log.debug("REST request to get a page of NodeCommodities");
        Page<NodeCommodityDTO> page = nodeCommodityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/node-commodities");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /node-commodities/:id : get the "id" nodeCommodity.
     *
     * @param id the id of the nodeCommodityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the nodeCommodityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/node-commodities/{id}")
    @Timed
    public ResponseEntity<NodeCommodityDTO> getNodeCommodity(@PathVariable Long id) {
        log.debug("REST request to get NodeCommodity : {}", id);
        Optional<NodeCommodityDTO> nodeCommodityDTO = nodeCommodityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nodeCommodityDTO);
    }

    /**
     * DELETE  /node-commodities/:id : delete the "id" nodeCommodity.
     *
     * @param id the id of the nodeCommodityDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/node-commodities/{id}")
    @Timed
    public ResponseEntity<Void> deleteNodeCommodity(@PathVariable Long id) {
        log.debug("REST request to delete NodeCommodity : {}", id);
        nodeCommodityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
