package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.ModelRunConfigService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ModelRunConfigDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ModelRunConfig.
 */
@RestController
@RequestMapping("/api")
public class ModelRunConfigResource {

    private final Logger log = LoggerFactory.getLogger(ModelRunConfigResource.class);

    private static final String ENTITY_NAME = "modelRunConfig";

    private final ModelRunConfigService modelRunConfigService;

    public ModelRunConfigResource(ModelRunConfigService modelRunConfigService) {
        this.modelRunConfigService = modelRunConfigService;
    }

    /**
     * POST  /model-run-configs : Create a new modelRunConfig.
     *
     * @param modelRunConfigDTO the modelRunConfigDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new modelRunConfigDTO, or with status 400 (Bad Request) if the modelRunConfig has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/model-run-configs")
    @Timed
    public ResponseEntity<ModelRunConfigDTO> createModelRunConfig(@Valid @RequestBody ModelRunConfigDTO modelRunConfigDTO) throws URISyntaxException {
        log.debug("REST request to save ModelRunConfig : {}", modelRunConfigDTO);
        if (modelRunConfigDTO.getId() != null) {
            throw new BadRequestAlertException("A new modelRunConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ModelRunConfigDTO result = modelRunConfigService.save(modelRunConfigDTO);
        return ResponseEntity.created(new URI("/api/model-run-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /model-run-configs : Updates an existing modelRunConfig.
     *
     * @param modelRunConfigDTO the modelRunConfigDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated modelRunConfigDTO,
     * or with status 400 (Bad Request) if the modelRunConfigDTO is not valid,
     * or with status 500 (Internal Server Error) if the modelRunConfigDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/model-run-configs")
    @Timed
    public ResponseEntity<ModelRunConfigDTO> updateModelRunConfig(@Valid @RequestBody ModelRunConfigDTO modelRunConfigDTO) throws URISyntaxException {
        log.debug("REST request to update ModelRunConfig : {}", modelRunConfigDTO);
        if (modelRunConfigDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ModelRunConfigDTO result = modelRunConfigService.save(modelRunConfigDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, modelRunConfigDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /model-run-configs : get all the modelRunConfigs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of modelRunConfigs in body
     */
    @GetMapping("/model-run-configs")
    @Timed
    public ResponseEntity<List<ModelRunConfigDTO>> getAllModelRunConfigs(Pageable pageable) {
        log.debug("REST request to get a page of ModelRunConfigs");
        Page<ModelRunConfigDTO> page = modelRunConfigService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/model-run-configs");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /model-run-configs/:id : get the "id" modelRunConfig.
     *
     * @param id the id of the modelRunConfigDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the modelRunConfigDTO, or with status 404 (Not Found)
     */
    @GetMapping("/model-run-configs/{id}")
    @Timed
    public ResponseEntity<ModelRunConfigDTO> getModelRunConfig(@PathVariable Long id) {
        log.debug("REST request to get ModelRunConfig : {}", id);
        Optional<ModelRunConfigDTO> modelRunConfigDTO = modelRunConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(modelRunConfigDTO);
    }

    /**
     * DELETE  /model-run-configs/:id : delete the "id" modelRunConfig.
     *
     * @param id the id of the modelRunConfigDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/model-run-configs/{id}")
    @Timed
    public ResponseEntity<Void> deleteModelRunConfig(@PathVariable Long id) {
        log.debug("REST request to delete ModelRunConfig : {}", id);
        modelRunConfigService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
