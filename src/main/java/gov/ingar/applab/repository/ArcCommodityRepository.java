package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ArcCommodity;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ArcCommodity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ArcCommodityRepository extends JpaRepository<ArcCommodity, Long> {

}
