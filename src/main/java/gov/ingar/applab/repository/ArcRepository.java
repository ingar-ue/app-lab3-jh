package gov.ingar.applab.repository;

import gov.ingar.applab.domain.Arc;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Arc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ArcRepository extends JpaRepository<Arc, Long> {

}
