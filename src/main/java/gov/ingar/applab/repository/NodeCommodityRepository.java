package gov.ingar.applab.repository;

import gov.ingar.applab.domain.NodeCommodity;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NodeCommodity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NodeCommodityRepository extends JpaRepository<NodeCommodity, Long> {

}
