# AppLab

## Development

To start your application in the dev profile, simply run both commands:

    ./mvnw
    
    yarn start


## Building for production

To optimize the AppLab application for production, run:

    ./mvnw clean package -Pprod -DskipTests


## Funcionamiento

El optimizador envía un POST Request al servidor Flask/PYOMO con 4 csv que contienen los datos 
de los nodos, los arcos y las relaciones de éstos con las mercancías.

Recibe como respuesta un JSON que luego es mapeado a una entidad Result. Esta entidad contiene:

* Interdicts: Es una lista de los arcos (Arc[]) que han sido atacados
* RemainingSupply

